// @flow
import React, { Component } from "react";
import { StatusBar } from "react-native";
import { StyleProvider } from "native-base";
import Expo,{ Font, AppLoading } from "expo";
import { useStrict } from "mobx";
import { Ionicons } from '@expo/vector-icons';
import HomeScreen from './src/HomeScreen';

import getTheme from "./native-base-theme/components";
import variables from "./native-base-theme/variables/commonColor";
export default class App extends Component {

    constructor() {

        super();
        
        this.state = {

            isReady: false

        };

        StatusBar.setHidden(true);

        console.disableYellowBox = true;
    }

    async componentWillMount() {

        await Expo.Font.loadAsync({

            Roboto: require("native-base/Fonts/Roboto.ttf"),
            Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
            Ionicons: require("native-base/Fonts/Ionicons.ttf")

        });

        this.setState({ isReady: true });
    }

    render() {

        if (!this.state.isReady) {

            return <Expo.AppLoading

                startAsync={this._loadResourcesAsync}
            />;
        }
        return <HomeScreen />;
    }
}

_loadResourcesAsync = async () => {
    return Promise.all([
        Asset.loadAsync([
            require('./assets/images/robot-dev.png'),
            require('./assets/images/robot-prod.png'),
        ]),
        Font.loadAsync([
            // This is the font that we are using for our tab bar
            Ionicons.font,
            // We include SpaceMono because we use it in HomeScreen.js. Feel free
            // to remove this if you are not using it in your app
            { 'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf') },
        ]),
    ]);
};

export { App };
