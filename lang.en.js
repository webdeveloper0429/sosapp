
const _FORGOT_PASSWORD = 'Forgot Password';
const _NEW_PASSWORD = 'New Password';
const _PROVIDE_PHONENUMBER = 'Please provide PhoneNumber';
const _PROVIDE_EMAIL = 'Please provide Email';
const _EMAIL_INVALID = 'Email format is Invalid.';
const _PROVIDE_PASSWORD = 'Please provide Password.';
const _PASSWORD_SHORT = 'The password is too short(the minimum number of characters is 4).';
const _PASSWORD_MISMATCH = 'Password Mismatch.';
const _ERROR = 'Error';
const _PLEASE_LOGIN = 'Please log in';
const _NEW_MESSAGE = 'New Message';
const _INBOX = 'Inbox';
const _SENT = 'Sent';
const _INBOX_MESSAGE = 'Inbox Messages';
const _SENT_MESSAGE = 'Sent Messages';
const _SETTING = 'Setting';
const _NAME = 'Name';
const _LAST_NAME = 'Last Name';
const _EMAIL = 'E-mail';
const _REGION = 'Region';
const _CITY_TOWN = 'City/Town';
const _PROFESSION = 'Profession';
const _GENDER = 'Gender';
const _VIEW_MAP = 'ViewMap';
const _MESSAGE = 'Message';
const _LOG_OUT = 'LogOut';
const _AUTH_FAILED = 'Authentication failed';
const _PHONENUMBER = 'PhoneNumber';
const _PASSWORD = 'Password';
const _CONFIRM_PASSWORD = 'Confirm Password';
const _WEAK = 'Weak';
const _AVERAGE = 'Average';
const _STRONG = 'Strong';
const _REGISTER = 'Register';
const _LOGIN = 'Log In';
const _RESCUER  = 'RESCUER';
const _MEDICAL  = 'Medical';
const _DISASTER  = 'Disaster';
const _CRASH  = 'Crash';
const _ACCIDENT  = 'Accident';
const _HELP  = 'Help';
const _EXPLOSION  = 'Explosion';
const _FUEL  = 'Fuel';
const _ELECTRICITY  = 'Electricity';
const _POISON  = 'Poison';
const _WATER  = 'Water';
const _EXTRA  = 'Extra';
const _CAR  = 'Car';
const _MOTOR  = 'Motor';
const _WHEEL  = 'Wheel';
const _TRUCK  = 'Truck';
const _BUS  = 'Bus';
const _RACE  = 'Race';
const _AIRCRAFT  = 'Aircraft';
const _HELICOPTER  = 'Helicopter';
const _BOAT  = 'Boat';
const _SHIP  = 'Ship';
const _CARGO  = 'Cargo';
const _FIRES  = 'Fires';
const _FLOOD  = 'Flood';
const _EARTHQUAKE  = 'Earthquake';
const _LANDSLIDE  = 'Landslide';
const _SMER_AND_STRONG_WINDS  = 'Smer and strong winds';
const _SNOW_BLIZZARDS_AND_AVALANCHES  = 'Snow blizzards and avalanches';
const _FRACTURE  = 'Fracture';
const _SOWING  = 'Sowing';
const _EPILEPTIC_SEIZURES  = 'Epileptic seizures';
const _SWOLLEN_TONGUE  = 'Swollen tongue';
const _SOLAR_SHOCK  = 'Solar shock';
const _HEAT_IMPACT  = 'Heat impact';
const _BURNING  = 'Burning';
const _FREEZING  = 'Freezing';
const _WE_ARE_INFORMATION  = 'We are in the information';
const _THERE_ARE  = 'There are ';
const _PERSONS_HERE  = ' persons here.';
const _TYPE  = 'Type';
const _FREQUENTLY_ASKED_QUESTIONS ='Frequently asked questions?';
const _BLOCK_USER ='Are you sure want to block the user';
const _SELECT_DESTINATION ='Please select destination!';
const _REPORT ='Report';
const _CLOSE ='Close';
const _SIGNAL_INFORMATION ='Signal Information';
const _OK ='OK';
const _CANCEL ='Cancel';
const _YES ='Yes';
const _NO ='No';









