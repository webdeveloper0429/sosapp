import React, { Component } from "react";
import Savior from "../savior";
import { Dimensions } from 'react-native'
import Profile from "../ProfileScreen/index.js";
import SideBar from "../SideBar/SideBar.js";
import Setting from "../setting";
import ViewMap from "../viewmap";
import Message from "../Newmessage";
import SOS from "../SOS";
import ForgotPassword from "../forgot-password";
import SignIn from "../signin";
import SignUp from "../sign-up";
import Detail from "../SignalClass/Detail";
import { DrawerNavigator, StackNavigator } from "react-navigation";
import Medical from "../SignalClass/Medical.js";
import Disaster from "../SignalClass/Disaster.js";
import Crash from "../SignalClass/Crash.js";
import Accident from "../SignalClass/Accident.js";
import Help from "../SignalClass/Help.js";

const { width, height } = Dimensions.get('window');
const HomeScreenRouter = DrawerNavigator(
  {
    Home: { screen: Savior },
    SignIn: { screen: SignIn },
    ViewMap: { screen: ViewMap },
    Setting: { screen: Setting },
    Profile: { screen: Profile },
    Message: { screen: Message },
    SOS: { screen: SOS },
    ForgotPassword: {
      screen: ForgotPassword,

      navigationOptions: {
        drawerLockMode: 'locked-closed'
      }

    },
    NewPassword: {
      screen: ForgotPassword,
      navigationOptions: {
        drawerLockMode: 'locked-closed'
      }
    },
    SignUp: {
      screen: SignUp,

      navigationOptions: () => ({
        drawerLockMode: 'locked-closed'
      })
    },
    LogOut: {
      screen: SignIn,

      navigationOptions: {
        drawerLockMode: 'locked-closed'
      }
    },
    Detail:{
      screen: Detail,
    },
    Medical:{
      screen: Medical,
    },
    Disaster:{
      screen: Disaster,
    },
    Crash:{
      screen: Crash,
    },
    Accident:{
      screen: Accident,
    },
    Help:{
      screen: Help,
    }


  },

  {
    contentComponent: props => <SideBar {...props} />,
    drawerPosition: "right",
    drawerWidth: width * 2 / 3
  }
);

export default HomeScreenRouter;
