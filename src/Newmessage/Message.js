import React, { Component } from 'react'

import {
    Container,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Button,
    NativeModules
} from "native-base";
import {
    TouchableHighlight,
    ToastAndroid,
    TextInput,
    Dimensions,
    ScrollView,
    KeyboardAvoidingView,
    Modal,
    ActivityIndicator,
    Clipboard,
    Image,
    Share,
    StatusBar,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native'

import Exponent, { Constants,DocumentPicker, ImagePicker, registerRootComponent } from 'expo';

import FileUploader from 'react-native-file-uploader'

import { StackNavigator } from 'react-navigation'

import Storage from 'react-native-key-value-store';

import api from '../Utils/api';

import helper from '../Utils/helper';

import UserTitle from '../components/UserTitle';

const { width, height } = Dimensions.get('window');
export default class Newmessage extends Component {

    state = {

        image: null,
        video: null,
        src_img: null,
        uploading: false,

        user_id:"",
        inMessage: [
            { 'name': 'Ben', 'content': 'aaaaaaaaaaaaa' }

        ],
        outMessage: [
            { 'name': 'Ben', 'content': 'aaaaaaaaaaaaa' }

        ],
        newMessageButton: {
            backgroundColor: 'green',
            width: '100%',
            padding: 5,
            marginTop: 10,
            borderRadius: 5
        },
        inboxButton: {
            backgroundColor: 'gray',
            width: '100%',
            padding: 5,
            marginTop: 10,
            borderRadius: 5
        },
        sentButton: {
            backgroundColor: 'gray',
            width: '100%',
            padding: 5,
            marginTop: 10,
            borderRadius: 5
        },
        newMessageView: {
            width: '100%',
            height: '100%',
        },
        inboxView: {
            width: '100%',
            height: '100%',
            display: 'none',
        },
        sentView: {
            width: '100%',
            height: '100%',
            display: 'none',
        }

    }

    constructor(props) {
        super(props);

        var ls = require('react-native-local-storage');
        ls.get('is_athenticated').then((is_athenticated) => {

            if (!is_athenticated) {
               
                this.props.navigation.navigate('SignIn')
            }

        });

    }

    componentDidMount() {

        var ls = require('react-native-local-storage');
		var user_id = ls.get('user_id').then((user_id) => {

            this.setState({user_id:user_id});
        })

    }


    newMessage = () => {

        this.setState({
            newMessageButton: {
                backgroundColor: 'green',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            inboxButton: {
                backgroundColor: 'gray',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            sentButton: {
                backgroundColor: 'gray',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            newMessageView: {
                width: '100%',
                height: '100%',
            },
            inboxView: {
                width: '100%',
                height: '100%',
                display: 'none',
            },
            sentView: {
                width: '100%',
                height: '100%',
                display: 'none',
            },
        })
    }

    inbox = () => {

        this.setState({
            newMessageButton: {
                backgroundColor: 'gray',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            inboxButton: {
                backgroundColor: 'green',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            sentButton: {
                backgroundColor: 'gray',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            newMessageView: {
                width: '100%',
                height: '100%',
                display: 'none',
            },
            inboxView: {
                width: '100%',
                height: '100%',
            },
            sentView: {
                width: '100%',
                height: '100%',
                display: 'none',
            },
        })
    }

    sent = () => {


        this.setState({
            newMessageButton: {
                backgroundColor: 'gray',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            inboxButton: {
                backgroundColor: 'gray',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            sentButton: {
                backgroundColor: 'green',
                width: '100%',
                padding: 5,
                marginTop: 10,
                borderRadius: 5
            },
            newMessageView: {
                width: '100%',
                height: '100%',
                display: 'none',
            },
            inboxView: {
                width: '100%',
                height: '100%',
                display: 'none',
            },
            sentView: {
                width: '100%',
                height: '100%',
            },
        })
    }

    sendButton = () => {

        let user_id;
        var ls = require('react-native-local-storage');

        ls.get('user_id').then((user_id) => {

            let message = this.state.message;

            let image_uri = this.state.image;

            let video_uri = this.state.video;

            this._sendMessage(image_uri,user_id,message,video_uri );
            
        })

    }
    viewInMessage = (content) => {
        alert(content)
    }
    viewOutMessage = (content) => {
        alert(content)
    }

    handelMessage = (text) => {

        this.setState({ message: text })
    }

    async save(value) {



        let key = await Storage.get("Sent");

        if (key) {

            await Storage.set('Sent', key + 1);

        }
        else {

            await Storage.set("Sent", 1)

        }

        key = 'Sent' + (key + 1)

        await Storage.set(key, value);

        this.state.outMessage.push({ name: key, content: value });

    }

    async load() {


        let key = await Storage.get("Sent");

        while (key > 0) {

            message = await Storage.get('Sent' + key);

            this.state.outMessage.push({ name: 'Sent' + key, content: message });

            key = key - 1;


        }

        key = await Storage.get("Recieve");

        while (key > 0) {

            message = await Storage.get('Recieve' + key);

            this.state.inMessage.push({ name: 'Recieve' + key, content: message });

            key = key - 1;

        }

    }

    async get_value(key) {

        value = await Storage.get(key, value);

    }

    render() {

        const { navigate } = this.props.navigation;
        let { image } = this.state;

        return (

            <View style={styles.container}>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.banner} >
                    <Image style={styles.logoImage}
                        source={require('../../assets/img/logo.png')} />
                    <Text style={styles.topTitle}>RESCUER</Text>
                    <Button style={styles.logoImage} transparent onPress={() => navigate("DrawerOpen")} >
                        <Icon name="md-menu" style={{ fontSize: width / 8, color: 'black' }} />
                    </Button>
                </TouchableOpacity>
                <UserTitle username={this.state.username} />
                <View style={styles.titleView}>
                    <Text style={styles.titleText}>Message</Text>
                </View>
                <View style={styles.bodyView}>
                    <View style={styles.leftView}>
                        <TouchableOpacity
                            style={this.state.newMessageButton}
                            onPress={() => this.newMessage()}>
                            <Text style={styles.buttonText}>
                                New Message
                              </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={this.state.inboxButton}
                            onPress={() => this.inbox()}>
                            <Text style={styles.buttonText}>
                                Inbox
                              </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={this.state.sentButton}
                            onPress={() => this.sent()}>
                            <Text style={styles.buttonText}>
                                Sent
                              </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.rightView}>
                        <View style={this.state.newMessageView}>
                            <View style={styles.topView}>
                                <Text style={styles.topText}>
                                    New Message
                                  </Text>
                            </View>
                            <View style={styles.sbodyView}>
                                <View style={styles.textAreaView}>
                                    <ScrollView>
                                        {this._maybeRenderImage()}
                                        {this._maybeRenderVideo()}
                                        {this._maybeRenderUploadingOverlay()}
                                        <KeyboardAvoidingView behavior="padding">
                                            <TextInput onChangeText={this.handelMessage} style={{ fontSize: 20, height: 200, backgroundColor: '#aaaaaa55' }} multiline={true} numberOfLines={5} />
                                        </KeyboardAvoidingView>

                                    </ScrollView>
                                </View>
                                <View style={styles.bottomView}>
                                    <View style={styles.imageButtonView}>
                                        {/* <TouchableHighlight onPress={() => { this._pickImage()}}> */}
                                        <TouchableHighlight onPress={() => { this._pickDocument()}}>
                                            <Image style={{ marginRight: 20, width: 30, height: 30 }} source={require('../../assets/img/fileButton.png')} />
                                        </TouchableHighlight>
                                        <TouchableHighlight onPress={() => { this._takePhoto() }}>
                                            <Image style={{ marginRight: 20, width: 30, height: 30 }} source={require('../../assets/img/cameraButton.png')} />

                                        </TouchableHighlight>
                                    </View>
                                    <View style={styles.imageButtonView}>
                                        <TouchableHighlight onPress={() => { this.sendButton() }}>
                                            <Image style={{ marginLeft: 20, width: 30, height: 30 }} source={require('../../assets/img/sendButton.png')} />
                                        </TouchableHighlight>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={this.state.inboxView}>
                            <View style={styles.topView}>
                                <Text style={styles.topText}>
                                    Inbox Messages
                              </Text>
                            </View>
                            <View style={styles.sbodyView}>
                                <ScrollView>
                                    {
                                        this.state.inMessage.map((item, index) => (
                                            <TouchableHighlight key={item.name} onPress={() => { this.viewInMessage(item.content) }}>
                                                <View style={styles.item}>
                                                    <View style={{ width: '20%' }}>
                                                        <Image style={{ width: 40, height: 40 }} source={require('../../assets/img/down.png')} />
                                                    </View>
                                                    <View style={{ width: '70%' }}>
                                                        <Text style={{ fontSize: 20 }}>{item.name}</Text>
                                                        <Text>{item.content}</Text>
                                                    </View>
                                                </View>
                                            </TouchableHighlight>
                                        ))
                                    }
                                </ScrollView>
                            </View>
                        </View>
                        <View style={this.state.sentView}>
                            <View style={styles.topView}>
                                <Text style={styles.topText}>
                                    Sent Messages
                                  </Text>
                            </View>
                            <View style={styles.sbodyView}>
                                <ScrollView>
                                    {
                                        this.state.outMessage.map((item, index) => (
                                            <TouchableHighlight key={item.name} onPress={() => { this.viewOutMessage(item.content) }}>
                                                <View style={styles.item}>
                                                    <View style={{ width: '20%' }}>
                                                        <Image style={{ width: 40, height: 40 }} source={require('../../assets/img/up.png')} />
                                                    </View>
                                                    <View style={{ width: '70%' }}>
                                                        <Text style={{ fontSize: 20 }}>{item.name}</Text>
                                                        <Text>{item.content}</Text>
                                                    </View>
                                                </View>
                                            </TouchableHighlight>
                                        ))
                                    }
                                </ScrollView>
                            </View>
                        </View>


                    </View>
                </View>

            </View>

        )
    }
    _maybeRenderUploadingOverlay = () => {
        if (this.state.uploading) {
            return (
                <View
                    style={[
                        StyleSheet.absoluteFill,
                        {
                            backgroundColor: 'rgba(0,0,0,0.4)',
                            alignItems: 'center',
                            justifyContent: 'center',
                        },
                    ]}>
                    <ActivityIndicator color="#fff" animating size="large" />
                </View>
            );
        }
    };

    _maybeRenderImage = () => {
        let { image } = this.state;
        if (!image) {
            return;
        }

        return (
            <View
                style={{
                    marginTop: 10,
                    marginBottom: 10,
                    width: 250,
                    borderRadius: 3,
                    elevation: 2,
                    shadowColor: 'rgba(0,0,0,1)',
                    shadowOpacity: 0.2,
                    shadowOffset: { width: 4, height: 4 },
                    shadowRadius: 5,
                }}>
                <View
                    style={{
                        borderTopRightRadius: 3,
                        borderTopLeftRadius: 3,
                        overflow: 'hidden',
                    }}>
                    <Image source={{ uri: image }} style={{ width: '90%', height: 150 }} />
                </View>

                <Text
                    onPress={this._copyToClipboard}
                    onLongPress={this._share}
                    style={{ paddingVertical: 10, paddingHorizontal: 10 }}>
                    {image}
                </Text>
            </View>
        );
    };

    _maybeRenderVideo = () => {
        let { video } = this.state;
        if (!video) {
            return;
        }

        return (
            <View
                style={{
                    marginTop: 10,
                    marginBottom: 10,
                    width: 250,
                    borderRadius: 3,
                    elevation: 2,
                    shadowColor: 'rgba(0,0,0,1)',
                    shadowOpacity: 0.2,
                    shadowOffset: { width: 4, height: 4 },
                    shadowRadius: 5,
                }}>
                <View
                    style={{
                        borderTopRightRadius: 3,
                        borderTopLeftRadius: 3,
                        overflow: 'hidden',
                    }}>
                    <Image source={{ uri: video }} style={{ width: '90%', height: 150 }} />
                </View>

                <Text
                    onPress={this._copyToClipboard}
                    onLongPress={this._share}
                    style={{ paddingVertical: 10, paddingHorizontal: 10 }}>
                    {video}
                </Text>
            </View>
        );
    };
    _share = () => {
        Share.share({
            message: this.state.image,
            title: 'Check out this photo',
            url: this.state.image,
        });
    };

    _copyToClipboard = () => {
        Clipboard.setString(this.state.image);
        alert('Copied image URL to clipboard');
    };

    _takePhoto = async () => {

        let pickerResult = await ImagePicker.launchCameraAsync({
            allowsEditing: true,
            aspect: [4, 3],
        });

        this._handleImagePicked(pickerResult);
    };

    _pickImage = async () => {
        let pickerResult = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4, 3],
        });

        this._handleImagePicked(pickerResult);
    };

    _handleImagePicked = async pickerResult => {
        let uploadResponse, uploadResult;

        try {
            this.setState({ uploading: true });

            if (!pickerResult.cancelled) {
                
                let user_id = this.state.user_id;

                this.setState({ image: pickerResult.uri });

                // uploadResponse = await uploadImageAsync(pickerResult.uri,user_id);
                // uploadResult = await uploadResponse.json();
                // this.setState({ image: uploadResult.location });
                // this.setState({ src_img: uploadResult.src_img});

                // console.log(uploadResult);
            }
        } catch (e) {
            console.log({ uploadResponse });
            console.log({ uploadResult });
            console.log({ e });
            alert('Upload failed, sorry :(');
        } finally {
            this.setState({ uploading: false });
        }
    };

    _sendMessage = async (image_uri,user_id,message,video_uri )=> {

        let uploadResponse, uploadResult;

        try {
                this.setState({ uploading: true });
                
                this.setState({ image: image_uri });

                uploadResponse = await _send(image_uri,user_id,message,video_uri);

                uploadResult = await uploadResponse.json();

                this.setState({ image: uploadResult.location });
                
                this.setState({ src_img: uploadResult.src_img});

                console.log(uploadResult);

        } catch (e) {
            console.log({ uploadResponse });
            console.log({ uploadResult });
            console.log({ e });
            alert('Upload failed, sorry :(');
        } finally {
            this.setState({ uploading: false });
        }
    };

    _pickDocument = async () => {
        let pickerResult = await DocumentPicker.getDocumentAsync({});
        
          alert(pickerResult.uri);
          
      console.log(pickerResult);

      this._handleVideoPicked(pickerResult);


	}

    _handleVideoPicked = async pickerResult => {
        let uploadResponse, uploadResult;

        try {
            this.setState({ uploading: true });

            if (!pickerResult.cancelled) {

                this.setState({ video: pickerResult.uri });
                // uploadResponse = await uploadVideoAsync(pickerResult.uri,this.state.user_id);
                // uploadResult = await uploadResponse.json();
                // this.setState({ image: uploadResult.location });
            }
        } catch (e) {
            console.log({ uploadResponse });
            console.log({ uploadResult });
            console.log({ e });
            alert('Upload failed, sorry :(');
        } finally {
            this.setState({ uploading: false });
        }
    };    





}


async function _send(uri,user_id,message,video_uri) {

    let apiUrl = "http://sosapp.studiowebdemo.com/ajax/message_mobile.php";

    let formData = new FormData();

    if(uri){
        let uriParts = uri.split('.');
        
        let fileType = uriParts[uriParts.length - 1];

        
        formData.append('photo', {
            uri,
            name: `photo.${fileType}`,
            type: `image/${fileType}`,
        });
    }

    if(video_uri){

        let video_uriParts = video_uri.split('.');

        let video_fileType = video_uriParts[video_uriParts.length - 1];

        formData.append('video', {
            video_uri,
            name: `video.${video_fileType}`,
            type: `video/${video_fileType}`,
        });        
    }

    formData.append('userid',user_id);
    formData.append('message',message);
   

    let options = {
        method: 'POST',
        body: formData,
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
    };

    return fetch(apiUrl, options);
}

async function uploadVideoAsync(uri,user_id) {

    let apiUrl = 'https://file-upload-example-backend-dkhqoilqqn.now.sh/upload';

    let uriParts = uri.split('.');
    let fileType = uri[uri.length - 1];

    let formData = new FormData();
    formData.append('photo', {
        uri,
        name: `video.${fileType}`,
        type: `video/${fileType}`,
    });

    formData.append('userid',user_id);
    formData.append('message',message);

    let options = {
        method: 'POST',
        body: formData,
        headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
        },
    };

    return fetch(apiUrl, options);
}

const styles = StyleSheet.create({
    banner: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'rgba(250,0,0,1)',
        width: '100%',
        height: height / 8

    },

    logoImage: {
        alignItems: 'center',
        width: '20%',
        height: height / 10,
        resizeMode: 'contain',
    },

    topTitle: {
        width: '60%',
        textAlign: 'center',
        fontSize: width / 10,
        fontWeight: '400',
        color: '#fff',
    },

    container: {
        marginTop: 0,
    },

    titleView: {
        width: '100%',
        height: height / 10,
        backgroundColor: 'gray',
    },
    titleText: {
        fontSize: width / 11,
        fontWeight: '300',
        padding: 10,
        marginLeft: 20,
    },
    bodyView: {
        width: '100%',
        height: height - height / 4,
        backgroundColor: '#cfd2d6',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    leftView: {
        width: '40%',
        height: '100%',
    },
    rightView: {
        width: '60%',
        height: '100%',
    },

    buttonText: {
        color: 'white',
        fontSize: 15,
        fontWeight: '100',
        textAlign: 'center'
    },

    topView: {
        width: '100%',
        height: '10%',
        alignItems: 'center',
        borderBottomColor: 'black',
        borderBottomWidth: 2,
    },

    topText: {
        fontSize: 20,
        fontWeight: '100',
    },

    sbodyView: {
        width: '100%',
        height: '90%',
        backgroundColor: 'white',
        borderRadius: 5,
    },

    textAreaView: {
        width: '100%',
        height: '75%',
    },

    bottomView: {
        width: '100%',
        height: '15%',
        backgroundColor: '#cfd2d690',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },

    imageButtonView: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },

    item: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        margin: 2,
        padding: 5,
        borderRadius: 10,
        backgroundColor: 'gray',
    }

})



