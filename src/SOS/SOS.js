import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    ImageButton,
    TouchableHighlight,
    TouchableOpacity,
    Alert,
    Dimensions,
    TouchableWithoutFeedback,
    Image,

} from 'react-native';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const ONE_MIN = 10;
const ONE_HOUR = 3600;
const DELAY = 2; 
import {
    Menu,
    MenuContext,
    MenuOptions,
    MenuOption,
    MenuTrigger,
    renderers,
} from 'react-native-popup-menu';

import { BaseContainer, Avatar, Images } from "../components";
import { Header, Container, Right, Icon } from 'native-base'

import { EvilIcons } from "@expo/vector-icons";
import variables from "../../native-base-theme/variables/commonColor";
import { Button } from "native-base";

import UserTitle from '../components/UserTitle';

import PropTypes from "prop-types";

let startInterval;
let btn = false;

export default class SOS extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            minutes: 0,
            seconds: 0,
            hours: 0,
            is_twelve: false,
            image_url:Images.ring


        };

        const {goBack} = this.props.navigation;

        this.startTime = this.startTime.bind(this);

        this.startTime();

    }
    

    componentDidMount() {


    }

    startTime() {
        btn = true;
        let newThis = this;
        newThis.state.is_twelve = false;
        function starting() {

            if (newThis.state.hours === DELAY - 1 && newThis.state.minutes === ONE_MIN ) {
                btn = false;
                newThis.setState({ hours: 0, minutes: 0, seconds: 0 });
                newThis.setState({image_url : Images.stop_ring});
                clearInterval(startInterval);
                newThis.state.is_twelve = true;

            }
            else if (newThis.state.minutes === ONE_MIN && newThis.state.seconds === ONE_MIN) {
                newThis.setState({ minutes: 0, hours: (newThis.state.hours + 1), seconds: 0 });
            }
            else if (newThis.state.seconds < ONE_MIN) {
                newThis.setState({ seconds: (newThis.state.seconds + 1) });
            }
            else {
                newThis.setState({ minutes: newThis.state.minutes + 1, seconds: 0 });
            }

        }
        if (!newThis.state.is_twelve) {
            startInterval = setInterval(starting, 1);
        }
        else {
            return;
        }
    }

    render() {
        const {navigate} = this.props.navigation
        return (

            <Container >

                <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.banner} >
                    <Image style={styles.logoImage}
                        source={require('../../assets/img/logo.png')} />
                    <Text style={styles.topTitle}>RESCUER</Text>
                    <Button style={styles.logoImage} transparent onPress={() => navigate("DrawerOpen")} >
                        <Icon name="md-menu" style={{ fontSize: width / 8, color: 'black' }} />
                    </Button>
                </TouchableOpacity>


                <View>
                    <UserTitle />
                    <View>
                        <TouchableOpacity disabled={btn}
                            onPress={() => this.props.navigation.navigate('Medical', { user: '' })}>
                            <Image source={this.state.image_url} style={styles.image} />
                        </TouchableOpacity>
                    </View>
                    <View style={styles.timer}>

                        <View style={styles.imageView}>
                            <Image style={{ width: 50, height: 50 }} source={require('../../assets/img/timer.png')} />
                        </View>

                        <View style={styles.timerText}>
                            <Text style={styles.timerTitle}>{`${this.state.hours <= 9 ? "0" + this.state.hours.toString() : this.state.hours}:${this.state.minutes <= 9 ? "0" + this.state.minutes.toString() : this.state.minutes}.${this.state.seconds <= 9 ? "0" + this.state.seconds.toString() : this.state.seconds}`}</Text>
                        </View>

                    </View>

                </View>
            </Container>

        );

    }

}




const styles = StyleSheet.create({

    banner: {        
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'rgba(250,0,0,1)',
        width: '100%',
        height: height / 8

    },

    logoImage: {
        alignItems: 'center',
        width: '20%',
        height: height / 10,
        resizeMode: 'contain',
    },

    topTitle: {
        width: '60%',
        textAlign: 'center', 
        fontSize: width / 10,
        fontWeight: '400',
        color: '#fff',
    },
  
    timer: {
        flexDirection: 'row',
        marginTop: 0,
        height: '60%',
        alignSelf: 'center',
        width: width,
        backgroundColor: 'white'


    },
    timerTitle: {
        fontSize: 30,
        fontWeight: "300",

    },
    timerText: {
        width: '65%',
        alignItems: 'flex-start',
    },

    imageView: {
        width: '35%',

        alignItems: 'flex-start',
    },
 
    image: {
        width: width * 1.1 ,
        height: width * 1.1,
        alignSelf: 'center',
        resizeMode: 'contain',
    },
  
});
