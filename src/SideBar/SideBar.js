import React from "react";
import { AppRegistry, Image, StatusBar } from "react-native";
import {
  Button,
  Text,
  Container,
  List,
  ListItem,
  Content,
  Icon
} from "native-base";
const routes = ["ViewMap", "Setting", "Message", "NewPassword", "LogOut"];
export default class SideBar extends React.Component {
  render() {
    return (
      <Container style={{ backgroundColor: '#000000aa' }} >
        <Content>
          <Image
            source={{
              uri: "./logo.png"
            }}
            style={{
              height: 120,
              alignSelf: "stretch",
              justifyContent: "center",
              alignItems: "center",
              width: 200
            }}
          >
            <Image
              square
              style={{ height: 80, width: 70 }}
              source={{
                uri: "./logo.png"
              }}
            />
          </Image>
          <List
            dataArray={routes}
            renderRow={data => {
              return (
                <ListItem
                  button
                  onPress={() => this.props.navigation.navigate(data)}
                >
                  <Text style={{ fontSize: 20, color: "#ffffff" }}>{data}</Text>
                </ListItem>
              );
            }}
          />
        </Content>
      </Container>

    );
  }
}
