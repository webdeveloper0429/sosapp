/**
 * @flow
 */

import React from 'react';
import { Platform, ScrollView, Dimensions, View, Image, Text, TouchableOpacity, StyleSheet } from 'react-native';

import {
    Container,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Button
} from "native-base"

import { SafeAreaView, TabNavigator } from 'react-navigation';
import { Images } from "../components";

const { width, height } = Dimensions.get("window");

const Disaster = {
    Fires: {
        name: 'Fires',
        sub: {
            
            Building: {
                sub_content: "Building",
                sub_sub:""
            },

            Forest: 
            {
                sub_content: "Forest (Mountains)",
                    sub_sub:{
                        Pirin: "Pirin",
                        Rila: "Rila",
                        Stara : "Stara Planina",
                        Vitosha: "Vitosha",
                        Belasitsa: "Belasitsa",
                        Another: "Another",

                    }                
            }
            ,
            Vehicles:
            {
                sub_content: "Vehicles",
                    sub_sub:{
                        Car: "Car",
                        Motor: "Motor",
                        Plane : "Plane",
                        Helicopter: "Helicopter",
                        Boat: "Boat",
                        Ship: "Ship",

                    }                
            } ,
            Polish:
            {
                sub_content: "Polish",
                sub_sub: {
                    Small: "Small area",
                    Average: " Average area",
                    Large: " Large area"         
                    
                }
            }
            ,
        }
    },
    Flooding: {
        name: 'Flooding',
        sub: {
            Small: "Small area",
            Average: " Average area",
            Large: " Large area"         
            
        }
    },
    Earthquake: {
        name: 'Earthquake',
        sub: ""
    },
    Landslide: {
        name: 'Landslide',
        sub: ""
    },
    Storm: {
        name: 'Storm and strong winds',
        sub: ""
    },
    Snowflakes: {
        name: 'Snowflakes and avalanches',
        sub: ""
    },
};

export default class DisasterScreen extends React.Component {

    state = {
        showsub: 0,
        showsub_sub: 0
    }    

    constructor(props) {
        super(props);

        var ls = require('react-native-local-storage');
        ls.get('is_athenticated').then((is_athenticated) => {

            if (!is_athenticated) {

                this.props.navigation.navigate('SignIn')
            }

        });

    }

    render() {
        const { navigate } = this.props.navigation
        return (

            <View style={styles.containerView}>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.banner} >
                    <Image style={styles.logoImage}
                        source={require('../../assets/img/logo.png')} />
                    <Text style={styles.topTitle}>RESCUER</Text>
                    <Button style={styles.logoImage} transparent onPress={() => navigate("DrawerOpen")} >
                        <Icon name="md-menu" style={{ fontSize: width / 8, color: 'black' }} />
                    </Button>
                </TouchableOpacity>
                <View style={styles.container}>

                    <View style={styles.leftView}>
                        <Button
                            onPress={() => navigate("Medical", { data: 'Medical' })}
                            style={styles.leftButton} >

                            <Icon name='ios-medkit' style={styles.Icon} />
                            <Text style={styles.tab_title}>Medical</Text>

                        </Button>

                        <Button
                            onPress={() => navigate("Disaster", { data: 'disaster' })}
                            style={styles.leftButton}
                        >
                            <Icon name='ios-bonfire' style={styles.Icon} />
                            <Text style={styles.tab_title}>Disaster</Text>
                        </Button>

                        <Button
                            onPress={() => navigate("Crash")}
                            style={styles.leftButton}
                        >
                            <Icon name='ios-car' style={styles.Icon} />
                            <Text style={styles.tab_title}>Crash</Text>
                        </Button>

                        <Button
                            onPress={() => navigate("Accident")}
                            style={styles.leftButton}
                        >
                            <Icon name="md-alert" style={styles.Icon} />
                            <Text style={styles.tab_title}>Accident</Text>
                        </Button>

                        <Button
                            onPress={() => navigate("Help")}
                            style={styles.leftButton}
                        >
                            <Icon name="ios-walk" style={styles.Icon} />
                            <Text style={styles.tab_title}>Help</Text>
                        </Button>
                    </View>
                    <View style={styles.rightView}>
                        <ScrollView>

                            {Object.keys(Disaster).map((routeName: string) => (
                                <View>
                                    <TouchableOpacity key={routeName} onPress={() => this._handleItems(Disaster[routeName]) }>
                                        <View style={styles.item}>
                                            <Text style={styles.title}>{Disaster[routeName].name}</Text>
                                        </View>
                                    </TouchableOpacity>

                                    {this._subMenu(Disaster[routeName])}
                                </View>
                                
                            ))}


                        </ScrollView>

                    </View >

                </View >
            </View >
        )



    }


    _detail(detail){

        this.props.navigation.navigate('Detail', { detailName:detail  , disaster_type: 'Disaster' ,sub_name:'Flooding'});
    }
    
    _subMenu(subMenu){

        if (this.state.showsub == 0 || subMenu.sub =="") {
            return;
        }
        else if(this.state.showsub == 1 && subMenu.name == "Fires" ) {

            return (

                Object.keys(Disaster["Fires"].sub).map((subName: string) => (
                    
                    <View>
                   
                        <TouchableOpacity  key={subName} onPress={() =>  this._handleSubItems(subMenu.sub[subName])}>
                            <View style={styles.subItem}>
                                <Text style={styles.title}>{subMenu.sub[subName].sub_content}</Text>
                            </View>
                        </TouchableOpacity>
                        {this._ssubMenu(subMenu.sub[subName])}
                    </View>
                ))
            
            );
        }
        else if(this.state.showsub == 2 && subMenu.name == "Flooding" ){
            return (

                Object.keys(Disaster["Flooding"].sub).map((subName: string) => (
                    <TouchableOpacity  key={subName} onPress={() => this._detail(subMenu.sub[subName]) }>
                        <View style={styles.subItem}>
                            <Text style={styles.title}>{subMenu.sub[subName]}</Text>
                        </View>
                    </TouchableOpacity>
                ))
            );

        }


    }

    _handleItems(subMenu){

        let subName = subMenu.name;
    
        if(subMenu.sub ==""){
            this.props.navigation.navigate('Detail', { detailName:subName  , disaster_type: 'Disaster',sub_name:""})
            return;
        }

        if( subName == 'Fires') {

            if(this.state.showsub == 1) {
                this.setState({showsub : 0});
            }
            else {
                 this.setState({showsub : 1});
            }
               
        }
        else {

            if(this.state.showsub == 2) {
                this.setState({showsub : 0});
            }
            else {
                 this.setState({showsub : 2});
            }

        }
        
    }

    _ssubMenu(ssubMenu){

        if (this.state.showsub_sub == 0 || ssubMenu.sub_sub =="") {

            return;
        }
        else if(this.state.showsub_sub == 1 && ssubMenu.sub_content == "Forest (Mountains)" ) {

            return (

                Object.keys(ssubMenu.sub_sub).map((subName: string) => (
                   
                    <TouchableOpacity  key={subName} onPress={() => this.props.navigation.navigate('Detail', { detailName: ssubMenu.sub_sub[subName], disaster_type: 'Disaster' ,sub_name:'Forest (Mountains)' })}>
                        <View style={styles.ssubItem}>
                            <Text style={styles.title}>{ssubMenu.sub_sub[subName]}</Text>
                        </View>
                    </TouchableOpacity>
                ))
            
            );
        }
        else if(this.state.showsub_sub == 2 && ssubMenu.sub_content == "Vehicles" ){
            return (

                Object.keys(ssubMenu.sub_sub).map((subName: string) => (
                    <TouchableOpacity  key={subName} onPress={() => this.props.navigation.navigate('Detail', { detailName: ssubMenu.sub_sub[subName] , disaster_type: 'Disaster',sub_name:'Vehicles' })}>
                        <View style={styles.ssubItem}>
                            <Text style={styles.title}>{ssubMenu.sub_sub[subName]}</Text>
                        </View>
                    </TouchableOpacity>
                ))
            );

        }
        else if(this.state.showsub_sub == 3 && ssubMenu.sub_content == "Polish" ){
            return (

                Object.keys(ssubMenu.sub_sub).map((subName: string) => (
                    <TouchableOpacity  key={subName} onPress={() => this.props.navigation.navigate('Detail', { detailName: ssubMenu.sub_sub[subName] , disaster_type: 'Disaster',sub_name:'Polish' })}>
                        <View style={styles.ssubItem}>
                            <Text style={styles.title}>{ssubMenu.sub_sub[subName]}</Text>
                        </View>
                    </TouchableOpacity>
                ))
            );

        }

    } 

    _handleSubItems(subMenu){

        let subName = subMenu.sub_content;
    
        if(subMenu.sub_sub ==""){
            this.props.navigation.navigate('Detail', { detailName:subName  , disaster_type: 'Disaster',sub_name:'Polish' })
            return;
        }

        if( subName == 'Forest (Mountains)') {

            
            if(this.state.showsub_sub == 1) {
                this.setState({showsub_sub : 0});
            }
            else {
                 this.setState({showsub_sub : 1});
            }
               
        }

        else if( subName == 'Vehicles') {

            
            if(this.state.showsub_sub == 2) {
                this.setState({showsub_sub : 0});
            }
            else {
                 this.setState({showsub_sub : 2});
            }
               
        }
        else if( subName == 'Polish') {

            
            if(this.state.showsub_sub == 3) {
                this.setState({showsub_sub : 0});
            }
            else {
                 this.setState({showsub_sub : 3});
            }
               
        }                
        
    }


}

const styles = StyleSheet.create({
    containerView: {
        width: width,
        height: height,
        flexDirection: 'column'
    },
    container: {
        width: width,
        height: height - height / 8,
        flexDirection: 'row'
    },
    leftView: {
        width: '30%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: '#000000AA',
    },
    rightView: {
        width: '70%',
        height: '100%',
        backgroundColor: 'rgba(14,14,14,10)',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: '#ddd',
        width: '70%',
        height: '100%',
    },
    image: {
        width: width / 7,
        height: height / 15,
        alignSelf: 'center',
        resizeMode: 'contain',
    },
    Icon: {
        fontSize: width / 6,
        color: 'white'
    },
    tab_title: {
        fontSize: width / 25,
        color: 'white'
    },

    leftButton: {
        height: "19.5%",
        width: "100%",
        flexDirection: 'column',
        backgroundColor: '#000000AA',
    },


    banner: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'rgba(250,0,0,1)',
        width: '100%',
        height: height / 8

    },

    logoImage: {
        alignItems: 'center',
        width: '20%',
        height: height / 10,
        resizeMode: 'contain',
    },

    topTitle: {
        width: '60%',
        textAlign: 'center',
        fontSize: width / 10,
        fontWeight: '400',
        color: '#fff',
    },

    item: {
        paddingHorizontal: width / 23.5,
        paddingVertical: 2,
        borderColor: '#fff',
        borderWidth: 5,
        borderRadius: 5,
        margin: 5,
    },

    title: {
        fontSize: width / 15,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#fff',
    },
    description: {
        fontSize: 13,
        color: '#999',
    },
    subItem: {
        
        paddingHorizontal: width / 23.5,
        paddingVertical: 2,
        borderColor: '#00f',
        borderWidth: 2,
        borderRadius: 5,
        margin: 5,
        marginLeft: 30,
    }
    ,
    ssubItem: {
        
        paddingHorizontal: width / 23.5,
        paddingVertical: 2,
        borderColor: '#0ff',
        borderWidth: 2,
        borderRadius: 5,
        margin: 5,
        marginLeft: 60,
    }

});

