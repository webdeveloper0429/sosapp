/**
 * @flow
 */

import React from 'react';
import { Platform, ScrollView, Dimensions, View, Image, Text, TouchableOpacity, StyleSheet } from 'react-native';

import {
    Container,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Button
} from "native-base"

import { SafeAreaView, TabNavigator } from 'react-navigation';
import { Images } from "../components";

const { width, height } = Dimensions.get("window");

const state = {
    anchor: false,
}

const Accident = {
    Explosion: {
        name: 'Explosion',
        description: 'a',
    },
    Fuel: {
        name: 'Fuel',
        description: 'b',
    },
    Electricity: {
        name: 'Electricity',
        description: 'c',
    },
    Poison: {
        name: 'Poison',
        description: 'c',
    },
    Water: {
        name: 'Water',
        description: 'c',
    },
    Extra: {
        name: 'Extra',
        description: 'c',
    },
};


export default class HelpScreen extends React.Component {

    constructor(props) {
        super(props);

        var ls = require('react-native-local-storage');
        ls.get('is_athenticated').then((is_athenticated) => {

            if (!is_athenticated) {

                this.props.navigation.navigate('SignIn')
            }

        });

    }


    render() {
        const { navigate } = this.props.navigation
        return (

            <View style={styles.containerView}>
                <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.banner} >
                    <Image style={styles.logoImage}
                        source={require('../../assets/img/logo.png')} />
                    <Text style={styles.topTitle}>RESCUER</Text>
                    <Button style={styles.logoImage} transparent onPress={() => navigate("DrawerOpen")} >
                        <Icon name="md-menu" style={{ fontSize: width / 8, color: 'black' }} />
                    </Button>
                </TouchableOpacity>
                <View style={styles.container}>

                    <View style={styles.leftView}>
                        <Button
                            onPress={() => navigate("Medical", { data: 'Medical' })}
                            style={styles.leftButton} >

                            <Icon name='ios-medkit' style={styles.Icon} />
                            <Text style={styles.tab_title}>Medical</Text>

                        </Button>

                        <Button
                            onPress={() => navigate("Disaster", { data: 'disaster' })}
                            style={styles.leftButton}
                        >
                            <Icon name='ios-bonfire' style={styles.Icon} />
                            <Text style={styles.tab_title}>Disaster</Text>
                        </Button>

                        <Button
                            onPress={() => navigate("Crash")}
                            style={styles.leftButton}
                        >
                            <Icon name='ios-car' style={styles.Icon} />
                            <Text style={styles.tab_title}>Crash</Text>
                        </Button>

                        <Button
                            onPress={() => navigate("Accident")}
                            style={styles.leftButton}
                        >
                            <Icon name="md-alert" style={styles.Icon} />
                            <Text style={styles.tab_title}>Accident</Text>
                        </Button>

                        <Button
                            onPress={() => navigate("Help")}
                            style={styles.leftButton}
                        >
                            <Icon name="ios-walk" style={styles.Icon} />
                            <Text style={styles.tab_title}>Help</Text>
                        </Button>
                    </View>
                    <View style={styles.rightView}>
                        <ScrollView>

                            {Object.keys(Accident).map((routeName: string) => (
                                <TouchableOpacity key={routeName} onPress={() => this.props.navigation.navigate('Detail', { detailName:Accident[routeName].name, disaster_type: 'Help' })}>
                                    <View style={styles.item}>
                                        <Text style={styles.title}>{Accident[routeName].name}</Text>
                                    </View>
                                </TouchableOpacity>
                            ))}

                        </ScrollView>

                    </View >

                </View >
            </View >
      )
    }
}


const styles = StyleSheet.create({
    containerView: {
        width: width,
        height: height,
        flexDirection: 'column'
    },
    container: {
        width: width,
        height: height - height / 8,
        flexDirection: 'row'
    },
    leftView: {
        width: '30%',
        height: '100%',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: '#000000AA',
    },
    rightView: {
        width: '70%',
        height: '100%',
        backgroundColor: 'rgba(14,14,14,10)',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: '#ddd',
        width: '70%',
        height: '100%',
    },
    image: {
        width: width / 7,
        height: height / 15,
        alignSelf: 'center',
        resizeMode: 'contain',
    },
    Icon: {
        fontSize: width / 6,
        color: 'white'
    },
    tab_title: {
        fontSize: width / 25,
        color: 'white'
    },

    leftButton: {
        height: "19.5%",
        width: "100%",
        flexDirection: 'column',
        backgroundColor: '#000000AA',
    },
    banner: {        
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'rgba(250,0,0,1)',
        width: '100%',
        height: height / 8

    },

    logoImage: {
        alignItems: 'center',
        width: '20%',
        height: height / 10,
        resizeMode: 'contain',
    },

    topTitle: {
        width: '60%',
        textAlign: 'center', 
        fontSize: width / 10,
        fontWeight: '400',
        color: '#fff',
    },
    item: {
        paddingHorizontal: width / 23.5,
        paddingVertical: 2,
        borderColor: '#fff',
        borderWidth: 5,
        borderRadius: 5,
        margin: 5,
    },
   
    title: {
        fontSize: width / 15,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#fff',
    },
    description: {
        fontSize: 13,
        color: '#999',
    },
});

