import React, { Component } from 'react';
import {
    Text,
    View,
    StyleSheet,
    Image,
    ImageButton,
    TouchableHighlight,
    TouchableOpacity,
    Alert,
    Dimensions,
    TouchableWithoutFeedback
} from 'react-native';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
import {
    Menu,
    MenuContext,
    MenuOptions,
    MenuTrigger,
    renderers,
} from 'react-native-popup-menu';
import { EvilIcons } from "@expo/vector-icons";
import variables from "../../native-base-theme/variables/commonColor";
import { Button } from "native-base";

const { Popover } = renderers
export default class UnLoggedTop extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isOpen: false
        }
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        })

    }

    updateMenu(isOpen) {
        this.setState({ isOpen })
    }

    render() {

        return (

            <View>

                <TouchableOpacity style={styles.banner}>

                    <Image source={require('../../assets/img/logo.png')} style={styles.image} />

                    <Text style={styles.title}>RESCUER</Text>    

                </TouchableOpacity>

            </View>
        );
    }
}


const styles = StyleSheet.create({
    banner: {        
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'rgba(250,0,0,1)',
        width: '100%',
        height: height / 8

    },
    image: {
        alignItems: 'center',
        width: '20%',
        height: height / 10,
        resizeMode: 'contain',
    },

    title: {
        width: '80%',
        paddingLeft: width / 12, 
        fontSize: width / 10,
        fontWeight: '400',
        color: '#fff',
    }

})
