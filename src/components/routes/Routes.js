import React from 'react'
import {  Scene,
          Router,
          Actions          
          } from 'react-native-router-flux'
import Signin from '../../signin'
import SignUp from '../../sign-up';
import {ForgotPassword} from "../../forgot-password";
import {ViewMap} from "../../viewmap";
import {Savior} from "../../savior";
import {PushNotify} from "../../push";
import {Setting} from "../../setting";
import {Timer} from "../../timer";
import {Message} from "../../message";

const Routes = () => (
  <Router>
    <Scene key = "root" hideNavBar={true}>
      <Scene key = "signin" component = {Signin} />
      <Scene key = "signup" component = {SignUp} />
      <Scene key = "forgotPassword" component = {ForgotPassword} />
      <Scene key = "home" component = {Savior}/>
      <Scene key = "viewMap" component = {ViewMap}  />
      <Scene key = "pushNotify" component = {PushNotify}  />
      <Scene key = "setting" component = {Setting}  />
      <Scene key = "timer" component = {Timer} />
      <Scene key = "message" component = {Message} />
    </Scene>
  </Router>
)

export default Routes