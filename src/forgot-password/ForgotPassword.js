import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, StyleSheet, Button, Dimensions, ScrollView, KeyboardAvoidingView } from 'react-native'
import { StackNavigator } from 'react-navigation';
import UnLoggedTop from '../components/UnLoggedTop';
import api from '../Utils/api';
const { width, height } = Dimensions.get('window');

class ForgotPassword extends Component {

    state = {
        forgot_flag: 1,
        title: 'Forgot Password',
        phoneNumber: '',
        email: '',
        password: '',
        repassword: '',
        inputPassword: {
            display: 'none',
            marginTop: 10,
            width: '90%',
            height: 50,
            fontSize: 25,
            borderBottomColor: 'black',
            borderBottomWidth: 1
        },
        inputRePassword: {
            display: 'none',
            marginTop: 10,
            width: '90%',
            height: 50,
            fontSize: 25,
            borderBottomColor: 'black',
            borderBottomWidth: 1
        },
        newPassButton: {
            backgroundColor: 'red',
            padding: 20,
            marginTop: 15,
            width: '70%',
            borderRadius: 5
        },
        container2: {
            display: 'none',
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
        },
        first: {
            width: '100%',
            height: 5,
            borderColor: 'black',
            borderWidth: 1,
            borderRadius: 5
        },

        second: {
            width: '100%',
            height: 5,
            borderColor: 'black',
            borderWidth: 1,
            borderRadius: 5
        },

        third: {
            width: '100%',
            height: 5,
            borderBottomColor: 'black',
            borderWidth: 1,
            borderRadius: 5
        },
    }


    handlePhoneNumber = (text) => {
        this.setState({ phoneNumber: text })
    }

    handleEmail = (text) => {
        this.setState({ email: text })
    }

    handlePassword = (text) => {
        var weak = 4;
        var average = 8;
        var strong = 12;
        var pass = text;
        if (pass.length < 4) {
            this.setState({
                first: {
                    width: '100%',
                    height: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
                second: {
                    width: '100%',
                    height: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
                third: {
                    width: '100%',
                    height: 5,
                    borderBottomColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
            })
        }
        if (pass.length >= 4) {
            this.setState({
                first: {
                    backgroundColor: 'yellow',
                    width: '100%',
                    height: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
                second: {
                    width: '100%',
                    height: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
                third: {
                    width: '100%',
                    height: 5,
                    borderBottomColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
            })
        }
        if (pass.length >= 8) {
            this.setState({
                first: {
                    backgroundColor: 'yellow',
                    width: '100%',
                    height: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
                second: {
                    backgroundColor: 'green',
                    width: '100%',
                    height: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
                third: {
                    width: '100%',
                    height: 5,
                    borderBottomColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
            })
        }
        if (pass.length >= 12) {
            this.setState({
                first: {
                    backgroundColor: 'yellow',
                    width: '100%',
                    height: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
                second: {
                    backgroundColor: 'green',
                    width: '100%',
                    height: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
                third: {
                    backgroundColor: 'blue',
                    width: '100%',
                    height: 5,
                    borderBottomColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
            })
        }

        this.setState({ password: text })
    }

    handleRePassword = (text) => {
        this.setState({ repassword: text })
    }

    newPassword = () => {

        if (this.state.forgot_flag == 1) {
            this.setState({
                forgot_flag: 0,
                title: 'New Password'
            });

        }
        else {
            this.setState({
                forgot_flag: 1,
                title: 'Forgot Password'

            });

        }

        this.setState({
            inputPassword: {
                marginTop: 10,
                width: '90%',
                height: 50,
                fontSize: 25,
                borderBottomColor: 'black',
                borderBottomWidth: 1
            },
            inputRePassword: {
                marginTop: 10,
                width: '90%',
                height: 50,
                fontSize: 25,
                borderBottomColor: 'black',
                borderBottomWidth: 1
            },
            newPassButton: {
                display: 'none',
                backgroundColor: 'red',
                padding: 20,
                marginTop: 15,
                width: '70%',
                borderRadius: 5
            },
            container2: {
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-around',
                alignItems: 'center',
            },
        })
    }

    register = () => {

        if (this.state.forgot_flag == 1) {

            try {

                var phoneNumber = this.state.phoneNumber;
                var email = this.state.email;
                var forgot_flag = this.state.forgot_flag;

                if (phoneNumber == '') {

                    alert('Please provide PhoneNumber');

                } else if (email == '') {

                    alert('Please provide Email');

                } else {

                    let data = {
                        'user_phone': phoneNumber,
                        'email': email,
                        'forgot_flag': forgot_flag
                    }

                    api.postApi('forgot_mobile.php', data)
                        .then((res) => {

                            if (res.status == '1') {

                                alert(res.msg);

                                var ls = require('react-native-local-storage');

                                ls.save('is_athenticated', true);

                                ls.save('user_id', res.user.id);

                                this.props.navigation.navigate('Home', { user: res.user });

                            } else {

                                alert(res.msg);
                            }

                        })
                        .catch((e) => {
                            console.error(e);
                            alert('Error1')


                        })

                }
            } catch (e) {
                console.error(e);
                this.setState({ phoneNumber: '' });
                this.setState({ password: '' });
                alert('Error');
            }



        }
        else {
            try {

                var phoneNumber = this.state.phoneNumber;
                var email = this.state.email;
                var password = this.state.password;
                var repassword = this.state.repassword;
                var forgot_flag = this.state.forgot_flag;
                var emailRegex = /^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$/;

                if (phoneNumber == '') {

                    alert('Please provide PhoneNumber');

                } else if (email == '') {

                    alert('Please provide Email');

                } else if (!emailRegex.test(email)) {

                    alert('Email format is Invalid.');

                } else if (password == '') {

                    alert('Please provide Password.');

                } else if (password.length < 4) {

                    alert('The password is too short(the minimum number of characters is 4).');

                } else if (password != repassword) {

                    alert('Password Mismatch.');

                } else {



                    let data = {
                        'user_phone': phoneNumber,
                        'email': email,
                        'user_password': password,
                        'forgot_flag': forgot_flag
                    }

                    api.postApi('forgot_mobile.php', data)
                        .then((res) => {
                            if (res.status == '1') {
                                alert(res.msg);
                            } else {
                                alert(res.msg);
                            }
                        })
                        .catch((e) => {
                            alert('Error1')
                        })
                }
            } catch (e) {
                this.setState({ phoneNumber: '' });
                this.setState({ email: '' });
                alert('Error');
            }
        }
    }





    render() {

        const { navigate } = this.props.navigation;

        return (

            <View style={styles.body}>
                <UnLoggedTop />


                <KeyboardAvoidingView behavior="padding" style={styles.container}>
                    <ScrollView style={styles.containerScroll}>
                        <View style={styles.container1}>

                            <Text style={styles.forgotText}>{this.state.title}</Text>

                        </View>

                        <View style={styles.container1}>

                            <TextInput style={styles.input}
                                underlineColorAndroid="transparent"
                                placeholder="PhoneNumber"
                                placeholderTextColor="gray"
                                keyboardType="numeric"
                                autoCapitalize="none"
                                returnKeyType="next"
                                onSubmitEditing={()=>this.emailInput.focus()}
                                onChangeText={this.handlePhoneNumber} />

                            <TextInput
                                style={styles.input}
                                underlineColorAndroid="transparent"
                                placeholder="Email"
                                placeholderTextColor="gray"
                                autoCapitalize="none"
                                returnKeyType="next"
                                onSubmitEditing={()=>this.passwordInput.focus()}
                                ref={(input) => this.emailInput = input}
                                onChangeText={this.handleEmail} />
                            <TextInput
                                style={this.state.inputPassword}
                                underlineColorAndroid="transparent"
                                placeholder="Password"
                                placeholderTextColor="gray"
                                autoCapitalize="none"
                                returnKeyType="next"
                                onSubmitEditing={()=>this.rePasswordInput.focus()}
                                ref={(input) => this.passwordInput = input}
                                secureTextEntry={true}
                                onChangeText={this.handlePassword} />
                            <TextInput
                                style={this.state.inputRePassword}
                                underlineColorAndroid="transparent"
                                placeholder="Confirm Password"
                                placeholderTextColor="gray"
                                autoCapitalize="none"
                                returnKeyType="done"
                                ref={(input) => this.rePasswordInput = input}
                                secureTextEntry={true}
                                onChangeText={this.handleRePassword} />

                            <TouchableOpacity
                                style={this.state.newPassButton}
                                onPress={() => this.newPassword()}>
                                <Text style={styles.buttonText}>
                                    New Password
                          </Text>
                            </TouchableOpacity>
                            <View style={this.state.container2}>
                                <View style={styles.viewPro}>
                                    <Text>Weak</Text>
                                    <View id='first' style={this.state.first}></View>
                                </View>
                                <View style={styles.viewPro}>
                                    <Text>Average</Text>
                                    <View id='second' style={this.state.second}></View>
                                </View>
                                <View style={styles.viewPro}>
                                    <Text>Strong</Text>
                                    <View id='third' style={this.state.third}></View>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                    <View style={styles.bottomContainer}>

                        <TouchableOpacity
                            style={styles.registerButton}
                            onPress={() => this.register()}
                        >
                            <Text style={styles.buttonText}>
                                Register
                          </Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>

            </View>
        )
    }
}
export default ForgotPassword

const styles = StyleSheet.create({

    body: {
        width: width,
    },

    container: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'transparent',
        marginTop: 0,
        height: height - height / 8
    },

    containerScroll: {
        width: '100%',
        height:height-height/4
    },

    container1: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
    },

    bottomContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: height / 8
    },


    forgotText: {
        marginTop: height / 50,
        textAlign: 'center',
        color: 'black',
        fontWeight: '200',
        fontSize: height / 18
    },

    input: {
        marginTop: 10,
        width: '90%',
        height: 50,
        fontSize: 25,
        borderBottomColor: 'black',
        borderBottomWidth: 1
    },


    registerButton: {
        backgroundColor: 'red',
        padding: 20,
        width: '70%',
        borderRadius: 5
    },

    buttonText: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center'
    }
})