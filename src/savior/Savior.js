/* @flow */

import React from 'react';
import { ScreenOrientation } from 'expo';

import registerForPushNotificationsAsync from '../Utils/registerForPushNotificationsAsync';
import { Notifications } from 'expo';
import * as firebase from 'firebase';
import Storage from 'react-native-key-value-store';
import Expo from 'expo';

ScreenOrientation.allow(ScreenOrientation.Orientation.ALL);

import {
    Platform,
    ScrollView,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    Modal,
    Text,
    View,
    Image,
    Dimensions,
} from 'react-native';


import {
    Container,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Button
} from "native-base"

import { SafeAreaView, StackNavigator } from 'react-navigation';

import { BaseContainer, Avatar, Images } from "../components";

import UserTitle from '../components/UserTitle';

function changeScreenOrientation() {

    Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);

}

export default class MainScreen extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            username: '',
            modalVisible: false,
            title: "",
            body: "",
        }
        changeScreenOrientation()
    }

    componentWillMount() {

        var ls = require('react-native-local-storage');
        ls.get('is_athenticated').then((is_athenticated) => {

            if (!is_athenticated) {
                alert("Please log in");
                setTimeout(() => this.props.navigation.navigate('SignIn'), 1);
            }

        });
    }

    componentDidMount() {

        this._notificationSubscription = this._registerForPushNotifications();

    }

    toggleModal(visible) {
        this.setState({ modalVisible: visible })
    }

    render() {
        const navigate = this.props.navigation.navigate
        return (
            <Container >
                <View>
                    <TouchableOpacity onPress={() => navigate('Home')} style={styles.banner} >
                        <Image style={styles.logoImage}
                            source={require('../../assets/img/logo.png')} />
                        <Text style={styles.topTitle}>RESCUER</Text>
                        <Button style={styles.logoImage} transparent onPress={() => navigate("DrawerOpen")} >
                            <Icon name="md-menu" style={{fontSize: width/8, color: 'black' }} />
                        </Button>
                    </TouchableOpacity>
                    <UserTitle username={this.state.username} />
                </View>
                <View style={{ flex: 1 }}>
                    <View>
                        <TouchableOpacity
                            onPress={() => navigate('Medical', { user: '' })}>
                            <Image source={Images.stop_ring} style={styles.image} />
                        </TouchableOpacity>
                    </View>
                </View>


                <View style={styles.container}>

                    <Modal
                        animationType={"slide"}
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => { console.log("Modal has been closed.") }}>

                        <View style={styles.modal}>
                            <View style={styles.modalView}>
                                <View style={styles.spaceView}></View>
                                <View style={styles.imgCircleView}>
                                    <Image style={{ width: 50, height: 50 }} source={require('../../assets/img/red.png')} />
                                </View>
                                <View style={styles.messageTextView}>
                                    <View style={styles.messageTitleView}>
                                        <Text style={styles.titleText}>
                                            {this.state.title}
                                        </Text>
                                    </View>
                                    <View style={styles.messageBodyView}>
                                        <Text style={styles.bodyText}>
                                            {this.state.body}
                                        </Text>
                                    </View>
                                </View>
                                <View style={styles.buttonView}>
                                    <View style={styles.imgButtonView}>

                                        <TouchableHighlight onPress={() => { this.toggleModal(!this.state.modalVisible) }}>
                                            <Image style={{ width: 70, height: 70 }} source={require('../../assets/img/checkmark.png')} />
                                        </TouchableHighlight>

                                        <TouchableHighlight onPress={() => { this.toggleModal(!this.state.modalVisible) }}>
                                            <Image style={{ width: 60, height: 60 }} source={require('../../assets/img/disabled.png')} />
                                        </TouchableHighlight>
                                    </View>
                                </View>

                            </View>
                        </View>
                    </Modal>
                </View>


            </Container>

        )
    }

    _registerForPushNotifications() {

        registerForPushNotificationsAsync();

        this._notificationSubscription = Notifications.addListener(

            this._handleNotification

        );
    }

    _handleNotification = (notification) => {

        this.state = {
            username: '',
            modalVisible: false,
            title: "",
            body: "",
        }

        this.userID = firebase.auth().currentUser.uid;

        this.setState({ title: notification.data.title });

        this.setState({ body: notification.data.body });


        this.save(this.state.title, this.state.body);

        firebase.database().ref('users/' + this.userID + '/notifications').push(notification.data);

        this.toggleModal(true);

    };


    async save(title, body) {


        let key = await Storage.get("Recieve");

        if (key) {

            await Storage.set('Recieve', key + 1);

        }
        else {

            await Storage.set("Recieve", 1);

        }

        key = 'Recieve' + (key + 1);

        await Storage.set(key, body);

    }

}

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({

    banner: {        
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'rgba(250,0,0,1)',
        width: '100%',
        height: height / 8

    },

    logoImage: {
        alignItems: 'center',
        width: '20%',
        height: height / 10,
        resizeMode: 'contain',
    },

    topTitle: {
        width: '60%',
        textAlign: 'center', 
        fontSize: width / 10,
        fontWeight: '400',
        color: '#fff',
    },

    container: {
        flex: 1,
        width: width,
        height: height - 70
    }, 
    
    modal: {
        flex: 1,
        alignItems: 'center',
        padding: 100
    },
    modalView: {
        width: 300,
    },

    spaceView: {
        width: '100%',
        height: 50
    },

    imgCircleView: {
        width: 100,
        height: 100,
        borderRadius: 50,
        backgroundColor: '#11202195',
        alignItems: 'center',
        justifyContent: 'center',
    },
    messageTextView: {
        width: '100%',
        height: 200,
        marginTop: -20,
        borderRadius: 5,
        backgroundColor: '#11202195',
    },
    buttonView: {
        height: 70,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    imgButtonView: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        height: '100%',
        width: '70%',
        marginTop: 2,
        borderRadius: 5,
        backgroundColor: '#11202195',
    },

    messageTitleView: {
        width: '100%',
        height: 50,
        alignItems: 'center'
    },
    messageBodyView: {
        width: '100%',
        alignItems: 'center'
    },
    titleText: {
        fontSize: 25,
        lineHeight: 40,
        color: 'white',
        fontWeight: '200',
    },
    bodyText: {
        fontSize: 20,
        lineHeight: 40,
        color: 'white',
        fontWeight: '100'
    },
 
    image: {
        width: width,
        height: width,
        alignSelf: 'center',
        marginTop: height / 12,
        resizeMode: 'contain',
    },

    title: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#444',
    },   
});
