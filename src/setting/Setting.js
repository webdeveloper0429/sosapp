import React, { Component } from 'react'
import { View,
		Text,
		Dimensions,
		TouchableOpacity,
		TextInput,
		StyleSheet,
		Image,
		ScrollView,
		Switch,
		processColor, 
		ActivityIndicator
	} from 'react-native'

import { StackNavigator } from 'react-navigation'
import {
		Container,
		Content,
		Header,
		Title,
		Left,
		Icon,
		Right,
		Button,
		Spinner

	} from "native-base";
import {ImagePicker} from 'expo';
import Modal from 'react-native-modal'
import api from '../Utils/api';
const { width, height } = Dimensions.get('window');
class Setting extends Component {

	constructor(props) {
		super(props);
		var ls = require('react-native-local-storage');
		ls.get('is_athenticated').then((is_athenticated) => {

			if (!is_athenticated) {
				alert("Please log in");
				this.props.navigation.navigate('SignIn')
			}
		}
		);

		this.state = {
			image: null,
			uploading: false,
			phone_number: '',
			name: '',
			last_name: '',
			email: '',
			region: '',
			city: '',
			profession: '',
			gender: true,
			user_id: "",
			isLoading : 0,
			user_img : "",
			upload_modal:false
		}

	}
	componentDidMount() {

		var ls = require('react-native-local-storage');
		var user_id = ls.get('user_id').then((user_id) => {

			let data = {
				'userid': user_id
			}

			this.setState({user_id:user_id});

			api.postApi('userInfo_mobile.php', data)
				.then((res) => {
					if (res.status == '1') {
						var signal_info = res.data[0];
						this.setState({ name: signal_info.first_name });
						this.setState({ phone_number: signal_info.mobile });
						this.setState({ last_name: signal_info.last_name });
						this.setState({ email: signal_info.email });
						this.setState({ region: signal_info.region });
						this.setState({ city: signal_info.city });
						this.setState({ gender: signal_info.gender });
						this.setState({ profession: signal_info.profession });
						this.setState({ user_img: signal_info.user_img_url});

					} else {
						alert(res.msg);
					}
				})
				.catch((e) => {
					alert('Error1')
				})
		})
	}

	update(){

		this.setState({isLoading : 1});

		let gender;
		if(this.state.gender){
			gender = 1;
		}
		else{
			gender = 0;
		}
		let data = {

			'userid'	: this.state.user_id,
			'name'		: this.state.name,
			'last_name'	: this.state.last_name,
			'email'		: this.state.email,
			'region'	: this.state.region,
			'city'		: this.state.city,
			'profession': this.state.profession,
			'gender'	: gender
		}		
		console.log(data);
		api.postApi('userInfo_update.php', data)
		.then((res) => {
			if (res.status == '1') {

				this.setState({isLoading : 0});

				var ls = require('react-native-local-storage');

				ls.save('first_name', this.state.name);

				alert("success");
			} else {
				alert(res.msg);
			}
		})
		.catch((e) => {
			
			alert("server error");
			this.setState({isLoading : 0});
		})	

	}	

	render() {
		const navigate = this.props.navigation.navigate
		let { image } = this.state;
		return (


			<View style={styles.container}>

				<TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.banner} >
					<Image style={styles.logoImage}
						source={require('../../assets/img/logo.png')} />
					<Text style={styles.topTitle}>RESCUER</Text>
					<Button style={styles.logoImage} transparent onPress={() => navigate("DrawerOpen")} >
						<Icon name="md-menu" style={{ fontSize: width / 8, color: 'black' }} />
					</Button>
				</TouchableOpacity>

				<ScrollView style={styles.containerScroll}>
					<View style={styles.title_view}>
						<Text style={styles.title_text}>
							Setting
                    	</Text>
					</View>
					<View style={styles.phone_view}>
						<Image style={styles.phone_img} source={require('../../assets/img/mobile_128.png')} />
						<Text style={styles.phone_number}>{this.state.phone_number}</Text>
					</View>
					{this._userImage()}
					<View style={styles.body}>
								
						<View style={styles.item}>
							<Text style={styles.text}  >
								{'Name :'}
							</Text>
							
							<TextInput style={styles.text_content}
									underlineColorAndroid="transparent"
									placeholder=""
									placeholderTextColor="gray"
									autoCapitalize="none"
									returnKeyType="done"
									onChangeText={(name) => this.setState({name})}
									value={this.state.name} />	
						</View>

						<View style={styles.item}>
							<Text style={styles.text} >
								{'Last Name :'}
							</Text>
							
							<TextInput style={styles.text_content}
									underlineColorAndroid="transparent"
									placeholder=""
									placeholderTextColor="gray"
									autoCapitalize="none"
									returnKeyType="done"
									onChangeText={(last_name) => this.setState({last_name})}
									value={this.state.last_name} />	
						</View>

						<View style={styles.item}>
							<Text style={styles.text}  >
								{'E-mail :'}
							</Text>
							
							<TextInput style={styles.text_content}
									underlineColorAndroid="transparent"
									placeholder=""
									placeholderTextColor="gray"
									autoCapitalize="none"
									returnKeyType="done"
									onChangeText={(email) => this.setState({email})}
									value={this.state.email} />	
						</View>

						<View style={styles.item}>
							<Text style={styles.text}  >
								{'Region :'}
							</Text>
							
							<TextInput style={styles.text_content}
									underlineColorAndroid="transparent"
									placeholder=""
									placeholderTextColor="gray"
									autoCapitalize="none"
									returnKeyType="done"
									onChangeText={(region) => this.setState({region})}
									value={this.state.region} />	
						</View>

						<View style={styles.item}>
							<Text style={styles.text}  >
								{'City/Town :'}
							</Text>
							
							<TextInput style={styles.text_content}
									underlineColorAndroid="transparent"
									placeholder=""
									placeholderTextColor="gray"
									autoCapitalize="none"
									returnKeyType="done"
									onChangeText={(city) => this.setState({city})}
									value={this.state.city} />	
						</View>

						<View style={styles.item}>
							<Text style={styles.text} >
								{'Profession :'}
							</Text>
							
							<TextInput style={styles.text_content}
									underlineColorAndroid="transparent"
									placeholder=""
									placeholderTextColor="gray"
									autoCapitalize="none"
									returnKeyType="done"
									onChangeText={(profession) => this.setState({profession})}
									value={this.state.profession} />	
						</View>

						{this._uploadImage()}

						<View style={styles.item}>
							<Text style={styles.text}  >
								{'Gender :'}
							</Text>

							<Switch
								style = {{marginLeft : 20, width : "30%"}}
								onValueChange = {(gender) => this.setState({gender})}
								value = {this.state.gender}/>

							<TouchableOpacity
								style={styles.updateButton}
								onPress={() =>this.update()}>
								{this.state.isLoading ? <Spinner color="white" /> : <Text style={styles.buttonText}>
									Update
								</Text>}
							</TouchableOpacity>

						</View>

					</View>
				</ScrollView>
			</View>

		)
	}

	_userImage() {


		if(this.state.user_img == "" ) {

			return(

				<TouchableOpacity style={styles.user_view} onPress={() =>this.setState({upload_modal: true})}>
					<Image style={styles.user_img}  source={require("../../assets/img/user_male_1.png")} />
					<Text style={styles.user_name}>{this.state.name}</Text>

				</TouchableOpacity>

			)
		}
		else {
			return(

				<TouchableOpacity style={styles.user_view} onPress={() =>this.setState({upload_modal: true})}>
					<Image style={styles.user_img}  source={{ uri: this.state.user_img }} />
					{this._maybeRenderUploadingOverlay()}
					<Text style={styles.user_name}>{this.state.name}</Text>

				</TouchableOpacity>
			)			
		}

	}

	_uploadImage()
	{

		
		return(
			<Modal isVisible={this.state.upload_modal}>
				<View style={styles.modal}>
					<Text style={{marginTop: height/20, fontSize: height/30, alignItems: 'center' , color:"#fff"}}>
						Select the source of image
					</Text>
					<View style={{flexDirection:'column', marginTop:height/20, width:'100%', alignItems:'center'}}>
						<TouchableOpacity	onPress={this._pickImage}>
							<Text style={{padding:10, fontSize:height/40 ,color : '#0fff'}}>Pick an image from camera roll</Text>
						</TouchableOpacity>
						<TouchableOpacity onPress={this._takePhoto} style={{padding:5}} >
							<Text style={{padding:10, fontSize:height/40 ,color : '#0fff'}}>Take a photo</Text>
						</TouchableOpacity>					
					</View>

					<Text style={{padding:10, fontSize:height/40 ,color : '#fff'}} onPress={() => {this.setState({upload_modal:false})}}>Close</Text>

				</View>
			</Modal>
		)
	}
	_maybeRenderUploadingOverlay = () => {
		if (this.state.uploading) {
		  return (
			<View
			  style={[
				StyleSheet.absoluteFill,
				{
				  backgroundColor: 'rgba(0,0,0,0.4)',
				  alignItems: 'center',
				  justifyContent: 'center',
				},
			  ]}>
			  <ActivityIndicator color="#fff" animating size="large" />
			</View>
		  );
		}
	  };
	
	  _maybeRenderImage = () => {
		let { image } = this.state;
		if (!image) {
		  return;
		}
	
		return (
		  <View
			style={{
			  marginTop: 30,
			  width: 250,
			  borderRadius: 3,
			  elevation: 2,
			  shadowColor: 'rgba(0,0,0,1)',
			  shadowOpacity: 0.2,
			  shadowOffset: { width: 4, height: 4 },
			  shadowRadius: 5,
			}}>
			<View
			  style={{
				borderTopRightRadius: 3,
				borderTopLeftRadius: 3,
				overflow: 'hidden',
			  }}>
			  <Image source={{ uri: image }} style={{ width: height / 15, height: height / 15 }} />
			</View>
	
			<Text
			  onPress={this._copyToClipboard}
			  onLongPress={this._share}
			  style={{ paddingVertical: 10, paddingHorizontal: 10 }}>
			  {image}
			</Text>
		  </View>
		);
	  };

	_share = () => {
		Share.share({
		  message: this.state.image,
		  title: 'Check out this photo',
		  url: this.state.image,
		});
	  };
	
	  _copyToClipboard = () => {

		Clipboard.setString(this.state.image);
		alert('Copied image URL to clipboard');
	  };
	
	  _takePhoto = async () => {
		
		let pickerResult = await ImagePicker.launchCameraAsync({
		  allowsEditing: true,
		  aspect: [4, 3],
		});
	
		this._handleImagePicked(pickerResult);
	  };
	
	  _pickImage = async () => {
		
		let pickerResult = await ImagePicker.launchImageLibraryAsync({
		  allowsEditing: true,
		  aspect: [4, 3],
		});
	
		this._handleImagePicked(pickerResult);
	  };
	
	  _handleImagePicked = async pickerResult => {
		let uploadResponse, uploadResult;
	
		try {
		  this.setState({ uploading: true });
	
		  if (!pickerResult.cancelled) {
			uploadResponse = await uploadImageAsync(pickerResult.uri, this.state.user_id);
			uploadResult = await uploadResponse.json();
			this.setState({ image: uploadResult.location });
			this.setState({ user_img: uploadResult.location });

			var ls = require('react-native-local-storage');

			ls.save('user_image', this.state.user_img);

		  }
		} catch (e) {
		  console.log({ uploadResponse });
		  console.log({ uploadResult });
		  console.log({ e });
		  alert('Upload failed, sorry :(');
		} finally {
		  this.setState({ uploading: false });
		}
	  };	


}


async function uploadImageAsync(uri, user_id) {

	let apiUrl = "http://sosapp.studiowebdemo.com/ajax/fileupload_mobile.php";

	let uriParts = uri.split('.');
	let fileType = uriParts[uriParts.length - 1];

	let formData = new FormData();
	formData.append('photo', {
	  uri,
	  name: `photo.${fileType}`,
	  type: `image/${fileType}`,

	});
	
	formData.append('userid',user_id);
  
	let options = {
	  method: 'POST',
	  body: formData,
	  headers: {
		Accept: 'application/json',
		'Content-Type': 'multipart/form-data',
	  },
	};
  
	return fetch(apiUrl, options);
  }

export default Setting

const styles = StyleSheet.create({

	banner: {        
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'rgba(250,0,0,1)',
        width: '100%',
        height: height / 8

    },

    logoImage: {
        alignItems: 'center',
        width: '20%',
        height: height / 10,
        resizeMode: 'contain',
    },

    topTitle: {
        width: '60%',
        textAlign: 'center', 
        fontSize: width / 10,
        fontWeight: '400',
        color: '#fff',
    },

	container: {
		flex: 1,
		backgroundColor: '#e5e6e8',
		width: width,
		height: height
	},

	item:{
		flexDirection : "row",
		justifyContent : "flex-start",
		padding: 10,
	},
	title_view: {
		marginTop: 20,
		width: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		height: 40,
	},
	title_text: {
		textAlign: 'center',
		color: 'black',
		fontWeight: 'bold',
		fontSize: height / 20,
	},
	phone_view: {
		marginTop: 10,
		width: '100%',
		alignItems: 'flex-start',
		height: height / 11,
		flexDirection: 'row',
		borderBottomColor: 'black',
		borderBottomWidth: 2
	},
	phone_img: {
		marginTop: 5,
		marginLeft: 20,
		width: height / 15,
		height: height / 15
	},
	phone_number: {
		textAlign: 'center',
		marginTop: 5,
		marginLeft: 30,
		fontWeight: '200',
		fontSize: height / 20
	},
	user_view: {
		marginTop: 10,
		width: '100%',
		alignItems: 'flex-start',
		height: height / 11,
		flexDirection: 'row',
		borderBottomColor: 'black',
		borderBottomWidth: 2
	},
	user_img: {
		marginTop: 5,
		marginLeft: 20,
		width: height / 15,
		height: height / 15,
	},
	user_name: {
		textAlign: 'center',
		marginTop: 5,
		marginLeft: 30,
		fontWeight: '200',
		fontSize: height / 20
	},
	body: {
		marginTop: 30,
		width: '100%'
	},
	text: {
		marginLeft: 10,
		fontWeight: '500',
		fontSize: 20,
	
	},
	text_content: {
		marginLeft: 10,

		fontWeight: '200',
		fontSize: 20,
		width : "60%"
	},	
	containerScroll: {
		width: '100%',
		height: height - height / 6
	},

    updateButton: {
		width: '35%',
        backgroundColor: 'green',
        padding: 5,
		borderRadius: 5
	},
		
    buttonText: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center'
	},
	
    modal: {
		backgroundColor: '#0001', 
		flexDirection:'column',       
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },


})