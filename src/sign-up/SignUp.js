import React, { Component } from 'react'
import { View, Text, TouchableOpacity, TextInput, StyleSheet, Dimensions, ScrollView, KeyboardAvoidingView } from 'react-native'

import api from '../Utils/api';
import helper from '../Utils/helper';
import * as firebase from 'firebase';
import { StackNavigator } from 'react-navigation';

import { Permissions, Notifications } from 'expo';
import UnLoggedTop from '../components/UnLoggedTop';
const { width, height } = Dimensions.get('window');

async function reg() {
    let token = await Notifications.getExpoPushTokenAsync();
    return token

}

class SignUp extends Component {

    state = {
        phoneNumber: '',
        email: '',
        password: '',
        repassword: '',
        first: {
            width: '100%',
            height: 5,
            borderColor: 'black',
            borderWidth: 1,
            borderRadius: 5
        },

        second: {
            width: '100%',
            height: 5,
            borderColor: 'black',
            borderWidth: 1,
            borderRadius: 5
        },

        third: {
            width: '100%',
            height: 5,
            borderBottomColor: 'black',
            borderWidth: 1,
            borderRadius: 5
        },

    }

    handlePhoneNumber = (text) => {
        this.setState({ phoneNumber: text })
    }

    handleEmail = (text) => {
        this.setState({ email: text })
    }

    handlePassword = (text) => {
        var weak = 4;
        var average = 8;
        var strong = 12;
        var pass = text;
        if (pass.length < 4) {
            this.setState({
                first: {
                    width: '100%',
                    height: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
                second: {
                    width: '100%',
                    height: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
                third: {
                    width: '100%',
                    height: 5,
                    borderBottomColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
            })
        }
        if (pass.length >= 4) {
            this.setState({
                first: {
                    backgroundColor: 'yellow',
                    width: '100%',
                    height: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
                second: {
                    width: '100%',
                    height: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
                third: {
                    width: '100%',
                    height: 5,
                    borderBottomColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
            })
        }
        if (pass.length >= 8) {
            this.setState({
                first: {
                    backgroundColor: 'yellow',
                    width: '100%',
                    height: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
                second: {
                    backgroundColor: 'green',
                    width: '100%',
                    height: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
                third: {
                    width: '100%',
                    height: 5,
                    borderBottomColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
            })
        }
        if (pass.length >= 12) {
            this.setState({
                first: {
                    backgroundColor: 'yellow',
                    width: '100%',
                    height: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
                second: {
                    backgroundColor: 'green',
                    width: '100%',
                    height: 5,
                    borderColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
                third: {
                    backgroundColor: 'blue',
                    width: '100%',
                    height: 5,
                    borderBottomColor: 'black',
                    borderWidth: 1,
                    borderRadius: 5
                },
            })
        }

        this.setState({ password: text })
    }

    handleRePassword = (text) => {
        this.setState({ repassword: text })
    }

    register = () => {

        try {

            var phoneNumber = this.state.phoneNumber;
            var email = this.state.email;
            var password = this.state.password;
            var repassword = this.state.repassword;
            var emailRegex = /^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$/;

            if (phoneNumber == '') {

                alert('Please provide PhoneNumber.');

            } else if (email == '') {

                alert('Please provide Email.');

            } else if (!emailRegex.test(email)) {

                alert('Email format is Invalid.');

            } else if (password == '') {

                alert('Please provide Password.');

            } else if (password.length < 6) {

                alert('The password is too short(the minimum number of characters is 6).');

            } else if (password != repassword) {

                alert('Password Mismatch.');

            } else {



                this.setState({ error: '', loading: true });
                const { email, password } = this.state;
                firebase.auth().createUserWithEmailAndPassword(email, password)

                    .then(() => {


                        reg().then(token => {

                            userID = firebase.auth().currentUser.uid;

                            firebase.database().ref('/users/' + userID).update({ token: token })

                                .then((res) => {

                                    let data = {
                                        'phone': phoneNumber,
                                        'email': email,
                                        'password': password,
                                        'token': token
                                    }

                                    api.postApi('register_mobile.php', data)
                                        .then((res) => {
                                            if (res.status == '1') {

                                                alert(res.msg)
                                                var ls = require('react-native-local-storage');

                                                ls.save('is_athenticated', true);

                                                ls.save('user_id', res.user.id);

                                                this.props.navigation.navigate('Home', { user: res.user });
                                            } else {
                                                alert(res.msg);
                                            }
                                        })
                                        .catch((e) => {
                                            alert('Error1')
                                        })
                                }

                                )



                        }
                        )


                    })
                    .catch(() => {
                        alert('Authentication failed');

                        this.setState({ error: 'Authentication failed', loading: false });

                    })

            }
        } catch (e) {
            alert('Error');
        }
    }

    render() {

        const { navigate } = this.props.navigation;

        return (

            <View style={styles.body}>
                <UnLoggedTop />
                <KeyboardAvoidingView behavior="padding" style={styles.keyBody}>
                <ScrollView style={styles.containerScroll}>
                    <View style={styles.container}>
                        <View style={styles.container2}>
                            <Text style={styles.registerText}>Register</Text>
                        </View>
                        <View style={styles.container2}>
                            <TextInput style={styles.input}
                                underlineColorAndroid="transparent"
                                placeholder="PhoneNumber"
                                placeholderTextColor="gray"
                                autoCapitalize="none"
                                keyboardType="numeric"
                                returnKeyType="next"
                                onSubmitEditing={()=>this.emailInput.focus()}
                                onChangeText={this.handlePhoneNumber} />
                            <TextInput style={styles.input}
                                underlineColorAndroid="transparent"
                                placeholder="Email"
                                placeholderTextColor="gray"
                                autoCapitalize="none"
                                returnKeyType="next"
                                onSubmitEditing={()=>this.passwordInput.focus()}
                                ref={(input) => this.emailInput = input}
                                onChangeText={this.handleEmail} />
                            <TextInput style={styles.input}
                                underlineColorAndroid="transparent"
                                placeholder="Password"
                                placeholderTextColor="gray"
                                autoCapitalize="none"
                                returnKeyType="next"
                                onSubmitEditing={()=>this.rePasswordInput.focus()}
                                ref={(input) => this.passwordInput = input}
                                secureTextEntry={true}
                                onChangeText={this.handlePassword} />
                            <TextInput style={styles.input}
                                underlineColorAndroid="transparent"
                                placeholder="Confirm Password"
                                placeholderTextColor="gray"
                                autoCapitalize="none"
                                returnKeyType="done"
                                ref={(input) => this.rePasswordInput = input}
                                secureTextEntry={true}
                                onChangeText={this.handleRePassword} />
                            <View style={styles.container1}>
                                <View style={styles.viewPro}>
                                    <Text>Weak</Text>
                                    <View id='first' style={this.state.first}></View>
                                </View>
                                <View style={styles.viewPro}>
                                    <Text>Average</Text>
                                    <View id='second' style={this.state.second}></View>
                                </View>
                                <View style={styles.viewPro}>
                                    <Text>Strong</Text>
                                    <View id='third' style={this.state.third}></View>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <View style={styles.bottomContainer}>
                    <TouchableOpacity
                        style={styles.registerButton}
                        onPress={() => this.register()}>
                        <Text style={styles.buttonText}>
                            Register
                                </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={styles.loginButton}
                        onPress={() => navigate('SignIn', { user: 'Lucy' })}>
                        <Text style={styles.buttonText}>
                            Log In
                                </Text>
                    </TouchableOpacity>
                </View>
                </KeyboardAvoidingView>
            </View>

        )
    }
}
export default SignUp

const styles = StyleSheet.create({

    body: {
        height: height
    },
    keyBody: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: height-height/8
    },

    container: {
        alignItems: 'center',
        backgroundColor: 'transparent',
        marginTop: 0,
        height: height - height / 4
    },

    containerScroll: {
        width: '100%',
    },
    container1: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    bottomContainer: {
        width: '100%',
        height: height / 8,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    container2: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
    },

    viewPro: {
        width: 70,
        height: 30,
        marginTop: 15,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
    },

    registerText: {
        marginTop: 20,
        textAlign: 'center',
        color: 'black',
        fontWeight: '200',
        fontSize: height / 18
    },

    input: {
        marginTop: 10,
        width: '90%',
        height: 50,
        fontSize: 25,
        borderBottomColor: 'black',
        borderBottomWidth: 1
    },

    loginButton: {
        width: '45%',
        backgroundColor: 'green',
        padding: 20,
        borderRadius: 5
    },

    registerButton: {
        width: '45%',
        backgroundColor: 'red',
        padding: 20,
        borderRadius: 5
    },

    buttonText: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center'
    }
})