import React, { Component } from 'react'
import { ActivityIndicator, View, Text, ToastAndroid, TouchableOpacity, TextInput, StyleSheet, Dimensions, ScrollView, Platform, KeyboardAvoidingView } from 'react-native'
import { StackNavigator } from 'react-navigation'
import { Button } from 'native-base'

import UnLoggedTop from '../components/UnLoggedTop';
import api from '../Utils/api';
import helper from '../Utils/helper';

const { width, height } = Dimensions.get('window');


function changeScreenOrientation() {

    Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);

}
class Signin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showToast: false,
            phoneNumber: '',
            password: '',
            loggedIn: false,
            loadedCookie: false
        };

        changeScreenOrientation();

    }

    componentWillMount() {

        var ls = require('react-native-local-storage');

        ls.remove('is_athenticated');

        ls.remove('user_id');

        ls.remove('first_name')

    }

    handlePhoneNumber = (text) => {
        this.setState({ phoneNumber: text })
    }

    handlePassword = (text) => {
        this.setState({ password: text })
    }
    login = () => {

        try {

            var phoneNumber = this.state.phoneNumber;
            var password = this.state.password;

            if (phoneNumber == '') {

                alert('Please provide PhoneNumber');

            } else if (password == '') {

                alert('Please provide Password');

            } else {

                let data = {
                    'user_phone': phoneNumber,
                    'user_password': password
                }

                api.postApi('login_mobile.php', data)
                    .then((res) => {

                        if (res.status == '1') {

                            var ls = require('react-native-local-storage');

                            ls.save('is_athenticated', true);

                            ls.save('user_id', res.user.id);

                            ls.save('first_name', res.user.first_name);

                            ls.save('user_image', res.user.user_img_url);


                            this.props.navigation.navigate('Home', { user: res.user });

                        } else {

                            alert(res.msg);
                        }

                    })
                    .catch((e) => {
                        alert('Error1');
                    })

            }
        } catch (e) {
            this.setState({ phoneNumber: '' });
            this.setState({ password: '' });
            alert('Error');
        }
    }

    render() {

        const { navigate } = this.props.navigation;

        return (
            <View style={styles.body}>
                <UnLoggedTop />
                <View style={styles.container}>
                    <KeyboardAvoidingView behavior="padding" style={styles.keyBody}>
                        <ScrollView style={styles.containerScroll}>
                            <View style={styles.container1}>

                                <Text style={styles.loginText}>Log In</Text>

                            </View>

                            <View style={styles.container1}>

                                <TextInput style={styles.input}
                                    underlineColorAndroid="transparent"
                                    placeholder="PhoneNumber"
                                    placeholderTextColor="gray"
                                    autoCapitalize="none"
                                    returnKeyType="next"
                                    onSubmitEditing={()=>this.passwordInput.focus()}
                                    keyboardType="numeric"
                                    onChangeText={this.handlePhoneNumber} />

                                <TextInput
                                    style={styles.input}
                                    underlineColorAndroid="transparent"
                                    placeholder="Password"
                                    placeholderTextColor="gray"
                                    autoCapitalize="none"
                                    returnKeyType="done"
                                    ref={(input) => this.passwordInput = input}
                                    secureTextEntry={true}
                                    onChangeText={this.handlePassword} />
                            </View>
                        </ScrollView>
                        <TouchableOpacity
                            style={styles.loginButton}
                            onPress={() => this.login()}>
                            <Text style={styles.buttonText}>
                                Log In
                            </Text>
                        </TouchableOpacity>
                    </KeyboardAvoidingView>

                    <View style={styles.container1}>
                        <TouchableOpacity
                            style={styles.registerButton}
                            onPress={() => navigate('SignUp', { user: 'Lucy' })}>
                            <Text style={styles.buttonText}>
                                Register
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.forgottenButton}
                            onPress={() => navigate('ForgotPassword', { user: 'Lucy' })}>
                            <Text style={styles.forgottenText}>
                                Forgotten Password?
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.container1}>

                        <TouchableOpacity
                            style={styles.questionButton}>
                            <Text style={styles.questionText}>
                                Frequently asked questions?
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}
export default Signin

const styles = StyleSheet.create({

    body: {
        width: width,
    },
    keyBody: {        
        alignItems: 'center',
        height: height / 2,
        width: width
    },
    container: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'transparent',
        marginTop: 0,
        height: height - height / 8
    },

    container1: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
    },

    containerScroll: {
        width: width,
        height:'80%'
    },

    loginText: {
        marginTop: 20,
        textAlign: 'center',
        color: 'black',
        fontWeight: '200',
        fontSize: height / 18
    },

    input: {
        marginTop: 10,
        width: '90%',
        height: 50,
        fontSize: 25,
        borderBottomColor: 'black',
        borderBottomWidth: 1
    },

    loginButton: {
        backgroundColor: 'green',
        width: '70%',
        padding: 20,
        marginTop: 5,
        borderRadius: 5
    },

    registerButton: {
        backgroundColor: 'red',
        width: '70%',
        padding: 20,
        marginTop: 5,
        borderRadius: 5
    },

    forgottenButton: {
        padding: 15,

    },

    forgottenText: {
        color: 'red',
        fontSize: 10,
        textAlign: 'center'
    },

    questionButton: {
        padding: 20,

    },

    questionText: {
        color: 'red',
        fontSize: 15,
        textAlign: 'center'
    },

    buttonText: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center'
    }
})