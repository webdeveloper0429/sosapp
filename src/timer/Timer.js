 import React, { Component } from 'react'
 import {Alert, View, Text, Dimensions,TouchableOpacity, TextInput, StyleSheet,Button,Image,ScrollView} from 'react-native'
 import moment from 'moment';
 import KeepAwake from 'react-native-keep-awake';
 import { StackNavigator } from 'react-navigation';

 import Top from '../components/Top';

  const { width, height } = Dimensions.get('window');

  class Timer extends Component {

        constructor(props) {
          super(props)
          this.state = { count: 0 ,
            time: moment().format("LTS"),
            date: moment().format("LL"),
            showMsg:false
          };
        }

        onPress = () => {
          this.setState({
          count: this.state.count+1,
          })
          Alert.alert('clicked')
        }
      
        render(){
         
        return (
          <ScrollView style = {styles.containerScroll}>
            <Header style={{flexDirection : 'row',  alignItems: 'center',  backgroundColor: 'rgb(250,0,0)',  width : '100%', height : height/6} } >       
              <Left>

              </Left>
              <TouchableOpacity  style={styles.banner} onPress = {() => navigate('Home')}>
                  <Image  style={{ marginLeft: 10,width: width/5, height: height/7,resizeMode: 'contain',padding: 5, }} 
                          source={require('../../assets/img/logo.png')} />
                
                <Text style={{ marginTop: 10,  marginLeft: 5,  fontSize: width/8,  fontWeight: '400', color: '#fff', }}>RESCUER</Text>
              </TouchableOpacity>
              <Button transparent onPress={() => this.props.navigation.navigate("DrawerOpen")} >
                <Icon name="md-menu" style={{fontSize: 50, color: 'black'}}/>
              </Button>

              <Right />
            </Header> 
            <View style = {styles.container}>
            
              <View style = {styles.saver}>
                <View style = {styles.imgView}>
                  <Image style = {{width:50,height:50}} source = {require('../../assets/img/user_male_1.png')} />
                </View>
                <View style = {styles.txtView}>
                  <Text style ={styles.userText}>Saver Name</Text>
                </View>
              </View>
              <View style = {styles.button}>
                <TouchableOpacity  onPress={this.onPress}>
                  <Image style = {{width:200,height:200}} source = {require('../../assets/img/sos.png')} />
                </TouchableOpacity> 
          
              </View>
              
              <View style = {styles.timer}>
                <View style = {styles.imgView}>
                  <Image style = {{width:50,height:50}} source = {require('../../assets/img/timer.png')}/>
                </View>
                <View style = {styles.txtView}>
                  <Text style ={styles.timeText}>{this.state.time}</Text>
                </View>
              </View> 

            </View>
          </ScrollView>    
          
         )

        }
    }
  export default Timer
   const styles = StyleSheet.create({
    banner: {
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: 'rgba(250,0,0,1)',
      width : '100%',
      height: height/6
  
    }, 
    body: {
      width:width,
      height:height-70
    },   
    container: {
          width:width,
          height:height-70,
          flexDirection: 'column',
          justifyContent: 'space-between',
          alignItems: 'center',
        },        
    saver: {
      flexDirection:'row',
      alignItems: 'center',
      justifyContent: 'space-around',
      height: 60,
    },
    button: {
      backgroundColor: '#e5e6e8',
      alignItems: 'center',
      justifyContent: 'center'
    },
    timer: {
      flexDirection:'row',
      height: 70,
      alignItems: 'flex-start',
      justifyContent: 'flex-start'
    },

    imgView: {
      width: '20%',
      height: '100%',
      alignItems: 'center',
    },
    txtView: {
      width: '80%',
      height: '100%',
    },
    userText: {
      fontSize: 40,
      fontWeight: '400',
    },
    timeText: {
      fontWeight: '300',
      fontSize: 30,
    }
  })