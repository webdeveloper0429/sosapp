import React from 'react';
import {
    Platform,
    ScrollView,
    Dimensions,
    View,
    Image,
    Text,
    TouchableOpacity,
    StyleSheet,
    Alert,
    TouchableHighlight
} from 'react-native';

import {
    Container,
    Content,
    Header,
    Title,
    Left,
    Icon,
    Right,
    Button
} from "native-base"

// import MapView from 'react-native-maps';
import { MapView } from 'expo';
import Polyline from '@mapbox/polyline';
import Modal from 'react-native-modal'
import api from '../Utils/api';
import UserTitle from '../components/UserTitle';
const DEFAULT_PADDING = { top: 40, right: 40, bottom: 40, left: 40 };
const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;

const SPACE = 0.01;

function createMarker(LATITUDE = 42.032923, LONGITUDE = 19.2, username = "RESCUER", user_id) {

    return {
        latitude: Number(LATITUDE),
        longitude: Number(LONGITUDE),
        username: username,
        user_id: user_id
    }

}

class StaticMap extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isInformationModalVisible: false,
            isReportUserModalVisible: false,
            origin: {
                latitude: '',
                longitude: ''

            },
            coords: [],
            destination: {
                latitude: '',
                longitude: ''
            },
            sos_signal: {},
            markers: [],
            user_id: '',

            username: '',
            modalVisible: false,
            title: "",
            body: "",

        }


    }

    componentWillMount() {

        var ls = require('react-native-local-storage');
        ls.get('is_athenticated').then((is_athenticated) => {
            if (!is_athenticated) {

                alert("Please log in");
                this.props.navigation.navigate('SignIn')
            }
            else {
                ls.get('first_name').then((first_name) => {

                })

                // setTimeout(() => this.fitAllMarkers(), 2000);
            }
        });

    }



    componentDidMount() {

        var ls = require('react-native-local-storage');

        ls.get('user_id').then((user_id) => {

            this.setState({ user_id: user_id });
            let data = {
                'userid': user_id
            }
            api.postApi('sos_mobile.php', data)
                .then((res) => {

                    if (res.status == '1') {

                        res.users.map((mark, index) => {
                            this.state.markers.push(createMarker(mark.latitude, mark.longitude, mark.first_name, mark.user_id));
                        })
                        navigator.geolocation.getCurrentPosition(
                            (position) => {
                                const initialPosition = JSON.stringify(position);
                                this.setState({ initialPosition });
                                let my_position = JSON.parse(initialPosition);
                                let my_info = createMarker(my_position.coords.latitude, my_position.coords.longitude, "ME", this.state.user_id);
                                this.setState({ origin: my_info });
                                this.setState({ destination: my_info });
                                this.state.markers.push(this.state.origin);

                            },
                            (error) => alert(error.message),
                            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
                        );
                        this.setState({
                            markers:
                                this.state.markers
                        })

                    } else {
                        alert(res.msg);
                    }
                })
                .catch((e) => {
                    console.error(e);
                    alert('Error1')
                })

        });
    }

    toggleModal(visible) {
        this.setState({ modalVisible: visible })
    }

    fitAllMarkers() {

        this.map.fitToCoordinates(this.state.markers, {
            edgePadding: { top: 50, right: 50, bottom: 200, left: 50 },
            animated: true

        });

    }

    isBlock() {
        Alert.alert(
            '',
            'Are you sure want to block the user',
            [
                {
                    text: 'OK', onPress: () => {

                        let data = {
                            'userid': this.state.select_user
                        }

                        api.postApi('block_mobile.php', data)
                            .then((res) => {

                                if (res.status == '1') {
                                    alert(res.msg);
                                } else {
                                    alert(res.msg);
                                }
                            })
                            .catch((e) => {
                                console.error(e);
                                alert('Error1')
                            })


                    }
                },
                { text: 'No', onPress: () => console.log('No '), style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    signalLocation() {

        this.setState({ marks: this.state.marks });
        setTimeout(() => this.fitAllMarkers(), 400);

    }

    fastestWay() {
        var origin = this.state.origin;
        var destination = this.state.destination;
        if (this.state.destination.latitude == "" || this.state.destination.longitude == "") {
            alert("Please select destination!");
            return;
        }

        this.setState([
            origin, destination
        ])

        setTimeout(() =>
            this.map.fitToCoordinates([this.state.origin, this.state.destination], {
                edgePadding: { top: 50, right: 50, bottom: 200, left: 50 },
                animated: true
            })
            , 400);
        let coords = this.getDirections(origin, destination);

    }

    async getDirections(startLoc, destinationLoc) {

        try {

            let resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${startLoc.latitude + "," + startLoc.longitude}&destination=${destinationLoc.latitude + "," + destinationLoc.longitude}`)
            let respJson = await resp.json();
            let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
            let coords = points.map((point, index) => {
                return {
                    latitude: point[0],
                    longitude: point[1]
                }
            })

            this.setState({ coords: coords })
            return coords
        } catch (error) {
            alert(error)
            return error
        }
    }

    showInformationModal = () => {



        var user_id = this.state.select_user;
        let data = {

            'userid': user_id

        }

        api.postApi('userInfo_mobile.php', data)
            .then((res) => {

                if (res.status == '1') {

                    var signal_info = res.data[0].comment;
                    this.setState({ signal_info: res.data[0].comment });


                } else {
                    alert(res.msg);
                }
            })
            .catch((e) => {
                console.error(e);
                alert('Error1')
            })

        this.setState({ isInformationModalVisible: true })

    }

    hideInformationModal = () => this.setState({ isInformationModalVisible: false })

    showReportModal = () => this.setState({ isReportModalVisible: true })

    hideReportModal = () => this.setState({ isReportModalVisible: false })

    showHospital() {

    }

    dismiss() {


    }

    selectMarker(destination, user_id) {


        this.setState({ destination: destination.destination });
        this.setState({ select_user: user_id });

    }

    account() {

        var user_id = this.state.select_user;
        let data = {

            'userid': user_id

        }

        api.postApi('userInfo_mobile.php', data)
            .then((res) => {

                if (res.status == '1') {

                    var user_info = res.data[0];

                    Alert.alert(
                        "Name :" + user_info.first_name + "  " + user_info.last_name,
                        'Mobile  :' + user_info.mobile,
                        [
                            {
                                text: 'Yes', onPress: () => {

                                }
                            }

                        ],
                        { cancelable: false }
                    )


                } else {
                    alert(res.msg);
                }
            })
            .catch((e) => {
                console.error(e);
                alert('Error1')
            })
    }

    onMapPress = (e) => {
        if (this.state.coordinates.length == 2) {
            this.setState({
                coordinates: [
                    e.nativeEvent.coordinate,
                ],
            });
        } else {
            this.setState({
                coordinates: [
                    ...this.state.coordinates,
                    e.nativeEvent.coordinate,
                ],
            });
        }
    }

    render() {
        const {navigate} = this.props.navigation

        return (

            <View>
                <View style={styles.body}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={styles.banner} >
                        <Image style={styles.logoImage}
                            source={require('../../assets/img/logo.png')} />
                        <Text style={styles.topTitle}>RESCUER</Text>
                        <Button style={styles.logoImage} transparent onPress={() => navigate("DrawerOpen")} >
                            <Icon name="md-menu" style={{ fontSize: width / 8, color: 'black' }} />
                        </Button>
                    </TouchableOpacity>
                    <UserTitle username={this.state.username} />
                    <View style={styles.container}>

                        <ScrollView
                            style={StyleSheet.absoluteFill}
                            contentContainerStyle={styles.scrollview}
                        >
                            <MapView
                                ref={ref => { this.map = ref; }}

                                provider={this.props.provider}
                                style={styles.map}
                                scrollEnabled={true}
                                zoomEnabled={true}
                                pitchEnabled={false}
                                rotateEnabled={false}

                                mapType='hybrid'
                                maxZoomLevel={20}
                                loadingEnabled={true}
                                showsUserLocation={true}
                            >

                                <MapView.Polyline
                                    coordinates={this.state.coords}
                                    strokeWidth={3}
                                    strokeColor="yellow" />

                                {this.state.markers.map((marker, i) => (
                                    <MapView.Marker
                                        key={i}
                                        coordinate={marker}
                                        onPress={() => {

                                            this.selectMarker({ destination: marker }, marker.user_id);
                                            this.setState({ username: marker.username });

                                        }

                                        }
                                    />
                                ))}



                            </MapView>

                        </ScrollView>
                        <View style={styles.top}>

                            <View style={styles.topToolbar} >


                                <TouchableOpacity onPress={() => alert(this.state.username)}>
                                    <Image style={styles.button} source={require('../../assets/img/user.png')} />

                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.account()}>
                                    <Text style={styles.username}> {this.state.username} </Text>
                                    <Image style={styles.button} source={require('../../assets/img/info.png')} />
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.isBlock()}>
                                    <Image style={styles.button} source={require('../../assets/img/block.png')} />
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => this.showReportModal()}>
                                    <Image style={styles.button} source={require('../../assets/img/report.png')} />
                                </TouchableOpacity>

                            </View>
                        </View>

                        <View style={styles.bottomToolbar} >

                            <TouchableOpacity onPress={() => { this.signalLocation() }}>
                                <Image style={styles.button} source={require('../../assets/img/location.png')} />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.fastestWay()}>
                                <Image style={styles.button} source={require('../../assets/img/fastest.png')} />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.showInformationModal()}>
                                <Image style={styles.button} source={require('../../assets/img/info.png')} />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.showHospital()}>
                                <Image style={styles.button} source={require('../../assets/img/hospital.png')} />
                            </TouchableOpacity>

                            <TouchableOpacity onPress={() => this.dismiss()}>
                                <Image style={styles.button} source={require('../../assets/img/dismiss.png')} />
                            </TouchableOpacity>

                            <Modal isVisible={this.state.isInformationModalVisible}>
                                <View style={styles.modal}>
                                    <Text style={{ fontSize: 25, alignItems: 'center' }}>
                                        Signal Information

                          </Text>
                                    <Text style={{ fontSize: 20, alignItems: 'center' }}>
                                        {this.state.signal_info}
                                    </Text>

                                    <Text style={styles.modal_button} onPress={() => { this.hideInformationModal() }}>Close</Text>

                                </View>
                            </Modal>

                            <Modal isVisible={this.state.isReportModalVisible}>
                                <View style={styles.modal}>
                                    <Text style={{ fontSize: 20 }}>
                                        This is a report
                          </Text>
                                    <Text style={styles.modal_button} onPress={() => { this.hideReportModal() }}>Close!</Text>

                                </View>
                            </Modal>

                        </View>
                    </View>
                </View>

            </View>

        );



    }


}

StaticMap.propTypes = {
    provider: MapView.ProviderPropType,
};



const styles = StyleSheet.create({

    banner: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        backgroundColor: 'rgba(250,0,0,1)',
        width: '100%',
        height: height / 8

    },

    logoImage: {
        alignItems: 'center',
        width: '20%',
        height: height / 10,
        resizeMode: 'contain',
    },

    topTitle: {
        width: '60%',
        textAlign: 'center',
        fontSize: width / 10,
        fontWeight: '400',
        color: '#fff',
    },
    body: {
        width: '100%',
        height: height
    },

    container: {

        flexDirection: 'column',
        flex: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'transparent',
        marginTop: 0,

    },
    scrollview: {
        alignItems: 'center'

    },
    top: {
        alignItems: 'center',
        width: "100%"

    },
    map: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        width: '100%',
        height: height
    },
    topToolbar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',

        backgroundColor: '#27282277',
        width: "100%",
        height: 60,

    },
    username: {
        color: "white",

    },
    bottomToolbar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        width: "100%",
        height: 60,
        backgroundColor: '#27282277',
    },

    button: {
        padding: 10,
        borderRadius: 5,
        width: 50,
        height: 50
    },

    modal: {
        backgroundColor: 'white',
        padding: 22,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },

    modal_button: {
        backgroundColor: 'lightblue',
        padding: 12,
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        borderColor: 'rgba(0, 0, 0, 0.1)',
    },

    noti_modal: {
        flex: 1,
        alignItems: 'center',
        padding: 100
    },

    modalView: {
        width: 300,
    },

    spaceView: {
        width: '100%',
        height: 50
    },

    imgCircleView: {
        width: 100,
        height: 100,
        borderRadius: 50,
        backgroundColor: '#11202195',
        alignItems: 'center',
        justifyContent: 'center',
    },

    messageTextView: {
        width: '100%',
        height: 200,
        marginTop: -20,
        borderRadius: 5,
        backgroundColor: '#11202195',
    },

    buttonView: {
        height: 70,
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
    },
    
    imgButtonView: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
        height: '100%',
        width: '70%',
        marginTop: 2,
        borderRadius: 5,
        backgroundColor: '#11202195',
    },

    messageTitleView: {
        width: '100%',
        height: 50,
        alignItems: 'center'
    },

    messageBodyView: {
        width: '100%',
        alignItems: 'center'
    },

    titleText: {
        fontSize: 25,
        lineHeight: 40,
        color: 'white',
        fontWeight: '200',
    },

    bodyText: {
        fontSize: 20,
        lineHeight: 40,
        color: 'white',
        fontWeight: '100'
    },

});

module.exports = StaticMap;
